# Articuno
Articuno is a C++ serialization framework that unifies concepts of internal and external serialization. Articuno's goal
is to solve the problem of different C++ frameworks, with differing capabilities, which implement serializing C++ types
to text/binary vs. those which implement reading existing document schemas into C++ types. Articuno provides a single
highly extensible API that can solve both problems.

## Major Features
* Support for both intrusive and non-intrusive serialization.
* Support for object tracking implemented in applicable archive backends, using native reference concepts (e.g. anchors
  and references in YAML).
* Support for polymorphic types, with customizable handling of how type information is integrated into the serialized
  document.
* Support for unified, symmetrical serialization/deserialization functions or split, separate serialization and
  deserialization functions.
* Out-of-the-box support for STL containers and other libraries, as well as some common third-party containers including
  Boost, Tessil maps, parallel-hashmap, libcds, libcuckoo, TBB, and others.
* Focus on standard formats: out-of-the-box support for major text formats such as JSON, XML, YAML, and TOML. Binary
  serialization is available with a proprietary format called IcyWind intended exclusively for internal serialization
  with advanced object tracking. Standard binary format support includes BSON, UBJSON, MessagePack, and CBOR.
* Implemented for C++20 and later, using the latest C++ features.
* Clean APIs, built on modern STL and Gluino.
* Extensible support for object allocation and initialization.
* Opaque handling of pointers, which allows for custom non-intrusive pointer serde handling (for pointers in
  general or for pointers to specific types).
* Transparent handling of multiple string encodings, with zero overhead where string encodings match what is
  expected.

## Minimum Requirements
* Articuno requires a minimum C++ version of C++20. It is recommended to use `c++latest` on MSVC due to certain C++20
  features only being enabled in C++23 mode.
* Some features only portable to Itanium and MSVC ABIs (validated on GCC, Clang, and MSVC).
* Articuno has two header-only dependencies: [Gluino](https://gitlab.com/colorglass/gluino) and
  [Parallel-Hashmap](https://github.com/greg7mdp/parallel-hashmap).

## Installation and Use
### Manual Install
Articuno is a header-only library, and can be included in a project by simply copying all headers into the project as
well as the headers of its dependencies [Gluino](https://gitlab.com/colorglass/gluino) and
[Parallel-Hashmap](https://github.com/greg7mdp/parallel-hashmap).

### Conan
Articuno builds are released on Conan. You must add a GitLab remote for Articuno's repository.

```console
conan remote add gitlab https://gitlab.com/api/v4/projects/33102313/packages/conan
```

You can then add Articuno to your project's `conanfile.txt` recipe:

```toml
[requires]
articuno/0.0.1@colorglass+articuno/stable

[generators]
cmake
```

### Vcpkg
Vcpkg is supported via the Color-Glass Studios repository. To add Articuno to your project, add
`vcpkg-configuration.json` in the project root (next to `vcpkg.json`) and add the following content:

```json
{
  "registries": [
    {
      "kind": "git",
      "repository": "https://gitlab.com/colorglass/vcpkg-colorglass",
      "baseline": "70edd3a6927bb508e8fa06f55c750a86e407a065",
      "packages": [
        "gluino",
        "articuno"
      ]
    }
  ]
}
```

You can then add `articuno` to your list of dependencies in `vcpkg.json`. You can also add Articuno globally (Vcpkg
classic mode) by adding the `vcpkg-configuration.json` file in your Vcpkg root.

## Simple Example
```c++
#include <articuno/archives/ryml/ryml.h>
#include <articuno/types/auto.h>

using namespace articuno;
using namespace articuno::ryml;

namespace {
    struct person {
        articuno_serde(ar) {
            ar <=> kv(name, "name");
            ar <=> kv(age, "age");
        }
        
        std::string name;
        uint32_t age;
    };
    
    struct people {
        articuno_serde(ar) {
            ar <=> kv(persons, "people");
        }
        
        std::vector<person> persons;
  };
}

int main() {
    people ppl;
    
    std::ifstream in("file.yaml");
    yaml_source src(in);
    src >> ppl;
    
    my_class.get_nested().set_age(25);
    
    std::ofstream out("file.yaml");
    yaml_sink snk(out);
    snk << ppl;
    
    return 0;
} 
```

## Keeping Ser/De Handlers Private
It is best practice to keep intrusive serialization hidden from a class' public API. Serialization can be kept private
and still accessible to Articuno by making `articuno::access` a friend class. Using the `articuno_super` macro to
call into parent class ser/de functions also routes the call through `articuno::access`, allowing even parent class
invocation from child classes while being kept fully private.

## Separate Serializers and Deserializers
Serializers and deserializers can be separated using concepts to limit which archives can invoke which function.
Articuno provides the `articuno::archive` concept to cover any archive, but `articuno::serializing_archive` and
`articuno::deserializing_archive` can match to exclusively serialization and deserialization respectively.

```c++
template <articuno::serializing_archive Archive, class T>
void serde(Archive& ar, articuno::value_flags flags) const {
    // Handle serialization.
}

template <articuno::deserializing_archive Archive, class T>
void serde(Archive& ar, articuno::value_flags flags) {
    // Handle deserialization.
}
```

If having a separate serialization function, it is recommended that it be made `const`, although this is not strictly
necessary. Articuno allows non-`const` serialization and combined ser/de handlers.

It is also possible to vary the behavior of a single function based on these concepts:

```c++
template <articuno::archive Archive, class T>
void serde(Archive& ar, articuno::value_flags flags) {
    // Common ser/de handling.
    if constexpr (articuno::serializing_archive<Archive>) {
        // Handle serialization.
    }
    if constexpr (articuno::deserializing_archive<Archive>) {
        // Handle deserialization.
    }
}
```

## Non-Intrusive Serialization
Non-intrusive ser/de handlers are supported by implementing a `serde` free function similar to the intrusive ones in the
`articuno::serde` namespace. These differ from the intrusive version only in taking an additional reference to the
target value. The `serde` function is overloaded via both argument and template concepts, which allows for overriding
less specific concepts with more specific ones, e.g. based on archive tags.

```c++
namespace articuno::serde {
    template <articuno::archive Archive, class T>
    requires (std::derived_from<T, person>)
    void serde(Archive& ar, T& value, articuno::value_flags flags) {
        ar <=> articuno::kv(value.name, "name");
        ar <=> articuno::kv(value.email, "email");
    }
}
```

Like intrusive ser/de methods, non-intrusive serializers can be divided between serialization and deserialization by
using specific concepts for the `Archive` template argument.

### Bundled Non-Intrusive Serializers
Articuno ships with an extremely extensive set of ser/de handlers for existing libraries, including full coverage of all
portably-serializable/deserializable STL types from C++23, as well as the two external libraries required for Articuno,
Gluino and Parallel Hashmap. In addition, it provides full-to-partial coverage for Tessil's hash maps and sets, the
robin-map, Boost, Qt5/6 types, and TBB collections.

The Articuno convention is to include specific handlers via `#include <articuno/types/<library>/<header>`, where the
header is named after the library's header file for the same type or types. There are also include-all options, e.g.
`#include <articuno/types/boost/boost.h>` will include all possible Boost types, as well as the "auto" option,
`#include <articuno/types/auto.h>`. The auto option will import all STL/C++ standard types, as well as Gluino and
Parallel Hashmap, and then bring it any other types which it detects were already included.

Special note should be given to the header files `articuno/types/stl/pointer.h` and `articuno/types/stl/zstring.h`.
These provide support for pointers and C-style strings respectively, and do not specifically map to STL classes.
Articuno treats pointers opaquely and so a non-intrusive pointer handler is required to support pointers.

## Polymorphic Types
Articuno has extensive, and highly customizable, polymorphism support. Polymorphism requires the use of indirection
(pointers or references) to polymorphic types. One feature of Articuno is that it treats such types *opaquely*, meaning
the framework does not inherently handle these types. By treating these opaquely it is possible to customize Articuno's
handling of pointers and references in general, a feature lacking in other such frameworks. This makes it possible for
Articuno to e.g. treat pointers in special ways. You may have a set of objects which are recreated on each execution,
but not themselves serializable. Articuno could still serialize these objects by some kind of unique ID, and then
deserialize them later by doing a lookup of the object instance by that ID. Out-of-the-box support for pointers and
references is provided with the non-intrusive serialization feature, via the headers `articuno/types/std/pointer.h` and
`articuno/types/std/reference.h`, although you are free to implement your own handlers.

If you use the default handlers, Articuno will follow indirection when serializing using the
`articuno::serde::dereference` function. This function can be overloaded by type and template concepts to customize
handling for specific types, for specific archives, or by specific archive tags (see below for more on ser/de tags). The
dereferencer's job is to help write the data with any additional information that is required to reconstruct on
deserialization. The default dereferencer will write an additional key `__type` which stores the demangled name of the
class. On deserialization these default implementations use the `articuno::serde::instantiate` function to create a new
instance of the polymorphic type, with the default implementation reading the same `__type` property. Because the
polymorphic type may be unknown to the deserializer, it may be necessary to register it explicitly. The default
instantiator uses a *type registry* to hold this information.

Types can be registered to the type registry by calling `articuno::types_for(ar)` and passing in an archive instance to
get an instance of the registry, and then calling `register_type<type_to_register>()` on it. The type registry instance
will be specific to each archive, and unique including all template options (e.g. the ser/de tags). If no instance of
the archive is available it is possible to get an instance of the registr with
`articuno::type_registry::get<Archive>()`. It is possible to get the type of an archive from a source or sink by using
`typename source_or_sink::archive_type`, e.g. `typename yaml_source::archive_type`.

### Customizing Type Registry Behavior
If using the default ser/de handlers, instantiators, and the type registry, you can still customize some type behaviors.
By default a type is registered to use a default constructor and a default allocator. If your type does not support
default construction, you can pass in an `std::function` to perform the in-place construction at a given memory address.

The memory address will be created by the types' allocator. By default, the allocator will be the function
`articuno::serde::allocator`. The allocator function can be overloaded by template concepts to customize it for
different types. However it is also possible to pass a totally custom allocator function to the `register_type` function
as well (including in addition to the custom constructor function).

## Sources, Sinks, and Archives
Articuno reads from *sources*, and writes to *sinks*. Unlike in many serialization frameworks for C++, these are
distinct from *archives*. Archives provide the APIs to translate data to and from documents, while sources and sinks are
top-level constructs which wrap input and output streams. Unlike archives, they have very specific APIs using stream
operators that only work in one direction.

```c++
void example() {
    std::ifstream input("file.yaml");
    articuno::ryml::yaml_source in(input);
    my_type data;
    in >> data;
    
    std::ofstream output("other_file.yaml");
    articuno::ryml::yaml_sink out(output);
    out << data;
}
```

### Bundled Archive Types
The following bundled archive types are supported:
* **RapidYAML**
  * YAML: `yaml_source`, `yaml_sink`
  * JSON: `yaml_source`, `json_sink` (`yaml_source` automatically detects and uses JSON, but the sink must be specific)
* **jsoncons**
  * JSON: `json_source`, `json_sink`
  * MessagePack: `msgpack_source`, `msgpack_sink`
  * CBOR: `cbor_source`, `cbor_sink`
  * UBJSON: `ubjson_source`, `ubjson_sink`
  * BSON: `bson_source`, `bson_sink`
* **TOML11**
  * TOML/INI: `toml_source`, `toml_sink`
* **PugiXML**
  * XML: `xml_source`, `xml_sink`
* **IcyWind** (IcyWind is a proprietary format for efficient non-human-readable, non-human-editable serialization; it is
  essentially a specific schema for object tracking support, built on top of jsoncons' MessagePack support, and should
  only be used when serializing data originating in C++ that you want to restore later, and not for reading data that
  originates from outside the application)
  * IcyWind: `icywind_source`, `icywind_sink`

The types here assume reading from input streams of `char`. Articuno supports various Unicode encodings wherever
possible, and most backends support streams of `char`, `wchar_t`, `char8_t`, `char16_t`, and `char32_t`. Aliases are
provided, e.g. `u8yaml_source` to read from a parser configured for UTF-8. Templates are also available to customize
the source parser in an ad-hoc way as well.

## Ser/De Tags
Tags are a way to arbitrarily alter the static type characteristics of archives, which allows for customizing the way in
which the C++ compiler will resolve invocations of ser/de handlers, instantiators, dereferencers, allocators, and other
extensible aspects of the framework. Sources and sinks accept a tag template argument, which is a value type which can
be a template value argument. On any archive type `Archive`, the type of the tag can be accessed with
`typename Archive::serde_tag_type`, and the value by `Archive::serde_tag`. Since the type and value are both static
constant expressions they can be used to add concept requirements to extensible functions to override default behaviors.

An example of how this can be used is in the out-of-the-box ser/de handlers for STL's chrono classes. Articuno provides
tags which customize the way in which classes such as `std::chrono::time_point` are handled, such as changing it from
mapping from time from epoch or a string, or, if it is time from epoch, in which units it should be interpreted.
