#pragma once

#include "concepts.h"
#include "value_flags.h"

#define articuno_serialize(ar, ...) template <::articuno::serializing_archive Archive> \
    void serde(Archive& ar, ::articuno::value_flags __VA_ARGS__) const

#define articuno_deserialize(ar, ...) template <::articuno::deserializing_archive Archive> \
    void serde(Archive& ar, ::articuno::value_flags __VA_ARGS__)

#define articuno_serde(ar, ...) template <::articuno::serializing_archive Archive> \
    void serde(Archive& __ar, ::articuno::value_flags __flags) const {               \
        const_cast<::std::remove_const_t<std::remove_pointer_t<decltype(this)>>*>(this)->__serde(__ar, ::std::move(__flags)); \
	}                                                                                \
                                                                                     \
    template <::articuno::deserializing_archive Archive>                             \
    void serde(Archive& __ar, ::articuno::value_flags __flags) {                     \
        __serde(__ar, ::std::move(__flags));                                         \
    }                                                                                \
                                                                                     \
    template <::articuno::archive Archive>                                           \
    void __serde(Archive& ar, ::articuno::value_flags __VA_ARGS__)

#define articuno_super(type, var, flags) ::articuno::access::serde<Archive, type>(var, *this, flags)
