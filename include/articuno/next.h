#pragma once

#include "value_flags.h"

namespace articuno {
	template <class Value>
	class next {
	public:
		using element_type = Value;
		using value_type = Value;

		constexpr explicit next(Value& value, ::articuno::value_flags flags = {}) noexcept
			: _value(&value), _flags(flags) {
		}

		[[nodiscard]] constexpr Value& value() noexcept {
			return *_value;
		}

		[[nodiscard]] constexpr ::articuno::value_flags flags() const noexcept {
			return _flags;
		}

	private:
		Value* _value;
		::articuno::value_flags _flags;
	};
}
