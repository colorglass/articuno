#pragma once

#include <gluino/concepts.h>

namespace articuno::tags {
	template <class T>
	concept complex_map_options = requires(const T& tag) {
		{ T::real_key } -> ::gluino::any_string_type;
		{ T::real_namespace } -> ::gluino::any_string_type;
		{ T::imag_key } -> ::gluino::any_string_type;
		{ T::imag_namespace } -> ::gluino::any_string_type;
	};

	struct default_complex_map {
		static constexpr ::std::string_view real_key = "real";
		static constexpr ::std::string_view real_ns = "";
		static constexpr ::std::string_view imag_key = "imag";
		static constexpr ::std::string_view imag_ns = "";
	};

	/**
	 * A tag to indicate that complex number structures should be serialized not as a pair, but as a map with two keys.
	 */
	template <::articuno::tags::complex_map_options T = default_complex_map>
	struct complex_map {
		using options_type = T;
	};
}
