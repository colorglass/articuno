#pragma once

#include <chrono>

namespace articuno::tags {
	/**
	 * A tag to indicate that ::std::chrono::duration should be serialized or deserialized as an integer representing time since epoch.
	 */
	template <class D = ::std::chrono::milliseconds>
	struct chrono_duration_epoch {
		using duration_type = D;
	};
}
