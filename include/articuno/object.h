#pragma once

#include "value_flags.h"

namespace articuno {
    class object {
    public:
        constexpr explicit object(::articuno::value_flags flags = {}) noexcept
            : _flags(flags) {
        }

        [[nodiscard]] constexpr ::articuno::value_flags flags() const noexcept {
            return _flags;
        }

    private:
        ::articuno::value_flags _flags;
    };
}
