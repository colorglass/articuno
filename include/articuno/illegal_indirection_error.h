#pragma once

#include "serde_error.h"

namespace articuno {
    class illegal_indirection_error : public ::articuno::logic_serde_error {
    public:
        using ::articuno::logic_serde_error::logic_serde_error;
    };
}
