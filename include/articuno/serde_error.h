#pragma once

#include <string>
#include <stdexcept>
#include <type_traits>

namespace articuno {
    template <class T>
    class serde_error : public T {
    public:
        inline explicit serde_error(const char* message) : T(message) {
        }

        inline explicit serde_error(::std::string_view message) : T(message.data()) {
        }

        inline explicit serde_error(const ::std::string& message) : T(message.c_str()) {
        }

        inline explicit serde_error(const char8_t* message)
                : T(reinterpret_cast<const char*>(message)) {
        }

        inline explicit serde_error(::std::u8string_view message)
                : T(reinterpret_cast<const char*>(message.data())) {
        }

        inline explicit serde_error(const ::std::u8string& message)
                : T(reinterpret_cast<const char*>(message.c_str())) {
        }

        inline serde_error(const serde_error& other) = default;
    };

    using runtime_serde_error = ::articuno::serde_error<::std::runtime_error>;
    using logic_serde_error = ::articuno::serde_error<::std::logic_error>;
}
