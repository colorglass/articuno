#pragma once

#include <cstdint>

#include <gluino/flags.h>

namespace articuno {
	namespace serde {
		gluino_flags(value_flags, uint64_t,
			(disable_tracking, 1 << 0),
			(disallow_narrowing, 1 << 1),
			(allow_narrowing, 1 << 2),
			(disallow_rounding, 1 << 3),
			(allow_rounding, 1 << 4),
			(required, 1 << 5),
			(nullable, 1 << 6),
			(allow_indirection, 1 << 7),
			(exclusive, 1 << 8),
			(attribute, 1 << 9),
			(literal, 1 << 10),
			(bitwise_binary, 1 << 11),
            (body_inlined, 1 << 12));
	}

	using value_flags = ::articuno::serde::value_flags;
}
