#pragma once

#include "kv.h"
#include "self.h"
#include "type_registry.h"

namespace articuno::serde {
	template <::articuno::serializing_archive Archive, class T>
	void dereference(Archive& ar, T* value, ::articuno::value_flags flags) {
		::articuno::types_for(ar).template register_type<T>();
		::articuno::serialize(ar, *value, flags);
	}

	template <::articuno::serializing_archive Archive, ::gluino::polymorphic T>
	void dereference(Archive& ar, T* value, ::articuno::value_flags flags) {
		auto tinfo = ::articuno::types_for(ar).lookup_type(*value);
		auto name = ::gluino::abi::demangle(::gluino::type_index(*value).name());
		if (tinfo.is_null()) {
			throw ::articuno::type_error(
				::std::format("Unable to find type information for polymorphic type '{}'.", name));
		}
		tinfo->serialize(ar, const_cast<void*>(reinterpret_cast<const void*>(value)), flags);
		ar <=> ::articuno::kv(name, "__type", flags);
	}

	template <::articuno::serializing_archive Archive, class T>
	requires (::articuno::primitive<T, Archive>)
	void dereference(Archive& ar, T* value, ::articuno::value_flags flags) {
		ar <=> ::articuno::self(*value, flags);
	}
}
