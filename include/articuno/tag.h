#pragma once

#include <chrono>

namespace articuno {
	/**
	 * The default serde tag, which the default non-intrusive serialization serde handlers will support.
	 */
	struct default_tag {
	};
}
