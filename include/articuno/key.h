#pragma once

#include <gluino/concepts.h>

namespace articuno {
	template <::gluino::any_string_type Name = ::std::string_view, ::gluino::any_string_type Namespace = ::std::string_view>
	class key {
	public:
		using namespace_type = Namespace;
		using name_type = Name;

		inline key(Name name) noexcept(noexcept(_name(name)))
			: _name(name) {
		}

		inline key(Namespace ns, Name name) noexcept(noexcept(_name(name)) && noexcept(_namespace(ns)))
			: _namespace(ns), _name(name) {
		}

		[[nodiscard]] inline const namespace_type& ns() const noexcept {
			return _namespace;
		}

		[[nodiscard]] inline const name_type& name() const noexcept {
			return _name;
		}

	private:
		namespace_type _namespace;
		name_type _name;
	};
}
