#pragma once

#include <ranges>

#include <gluino/concepts.h>

#include "value_flags.h"

namespace articuno {
	template <class Value, ::gluino::string_like Key = ::std::string_view,
	        ::gluino::string_like Namespace = ::std::string_view>
	class index {
	public:
		using key_type = Key;
		using index_type = ::std::make_signed_t<::std::size_t>;
		using element_type = Value;
		using value_type = Value;
		using namespace_type = Namespace;

		constexpr index(index_type index, element_type& value, ::articuno::value_flags flags = {}) noexcept
			: _index(index), _value(&value), _flags(::std::move(flags)) {
		}

		constexpr index(namespace_type nameSpace, key_type key, index_type index, element_type& value,
			::articuno::value_flags flags = {}) noexcept
			: _key(key), _namespace(nameSpace), _index(index), _value(&value), _flags(::std::move(flags)) {
		}

		constexpr index(key_type key, index_type index, element_type& value,
			::articuno::value_flags flags = {}) noexcept
			: _key(key), _index(index), _value(&value), _flags(::std::move(flags)) {
		}

		[[nodiscard]] inline constexpr const index_type& position() const noexcept {
			return _index;
		}

		[[nodiscard]] constexpr element_type& value() noexcept {
			return *_value;
		}

		[[nodiscard]] constexpr const element_type& value() const noexcept {
			return *_value;
		}

		[[nodiscard]] const namespace_type& key_namespace() const noexcept {
			return _namespace;
		}

		[[nodiscard]] const key_type& key() const noexcept {
			return _key;
		}

		[[nodiscard]] constexpr ::articuno::value_flags flags() const noexcept {
			return _flags;
		}

	private:
		index_type _index;
		element_type* _value;
		namespace_type _namespace;
		key_type _key{};
		::articuno::value_flags _flags;
	};
}
