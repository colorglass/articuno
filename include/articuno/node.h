#pragma once

#include <string_view>
#include <variant>
#include <vector>

#include <parallel_hashmap/phmap.h>

#include "access.h"
#include "kv.h"
#include "null.h"
#include "object.h"
#include "self.h"
#include "sequence.h"
#include "utility.h"

namespace articuno {
    template <class Char = char, class Traits = ::std::char_traits<Char>,
              template <class> class Alloc = ::std::allocator, template <class, class> class Sequence = ::std::vector,
              template <class, class, class, class, class> class Map = ::phmap::flat_hash_map>
    class node {
    public:
        using char_type = Char;

        using traits_type = Traits;

        template <class T>
        using alloc_type = Alloc<T>;

        using char_alloc_type = alloc_type<char_type>;

        using string_type = ::std::basic_string<char_type, traits_type, char_alloc_type>;

        using node_alloc_type = Alloc<node>;

        using sequence_type = Sequence<node, Alloc<node>>;

        using map_type = Map<string_type, node, ::gluino::str_hash<>, ::gluino::str_equal_to<>,
                             alloc_type<::std::pair<string_type, node>>>;

        constexpr node() noexcept = default;

        [[nodiscard]] constexpr bool is_null() const noexcept {
            return ::std::holds_alternative<nullptr_t>(_value);
        }

        [[nodiscard]] constexpr bool is_signed_int() const noexcept {
            return ::std::holds_alternative<int64_t>(_value);
        }

        [[nodiscard]] constexpr bool is_unsigned_int() const noexcept {
            return ::std::holds_alternative<uint64_t>(_value);
        }

        [[nodiscard]] constexpr bool is_int() const noexcept {
            return is_signed_int() || is_unsigned_int();
        }

        [[nodiscard]] constexpr bool is_float() const noexcept {
            return ::std::holds_alternative<double_t>(_value);
        }

        [[nodiscard]] constexpr bool is_string() const noexcept {
            return ::std::holds_alternative<string_type>(_value);
        }

        [[nodiscard]] constexpr bool is_sequence() const noexcept {
            return ::std::holds_alternative<sequence_type>(_value);
        }

        [[nodiscard]] constexpr bool is_map() const noexcept {
            return ::std::holds_alternative<map_type>(_value);
        }

        template <class T>
        [[nodiscard]] inline bool is() const noexcept {
            return ::std::visit([&](const auto&& val) {
                using val_type = ::std::remove_cvref_t<decltype(val)>;
                return ::std::convertible_to<val_type, T>;
            }, _value);
        }

        template <class T>
        [[nodiscard]] inline T get() const noexcept {
        }

        template <class T>
        [[nodiscard]] inline T get_or(T) const noexcept {
        }

    private:
        articuno_serialize(ar, flags) {
            ::std::visit([&](const auto&& val) {
                using val_type = ::std::remove_cvref_t<decltype(val)>;
                if constexpr (::std::same_as<val_type, sequence_type>) {
                    for (auto& entry : val) {
                        ar <=> ::articuno::next(entry, flags);
                    }
                } else if constexpr (::std::same_as<val_type, map_type>) {
                    for (auto& entry : val) {
                        ar <=> ::articuno::kv(entry.second, entry.first, flags);
                    }
                } else if constexpr (::std::same_as<val_type, nullptr_t>) {
                    ar <=> ::articuno::null();
                } else {
                    ar <=> ::articuno::self(val, flags);
                }
            }, _value);
        }

        articuno_deserialize(ar, flags) {
            if (ar <=> ::articuno::object()) {
                auto& value = _value.template emplace<map_type>();
                ar <=> ::articuno::self(value, flags);
            } else if (ar <=> ::articuno::sequence()) {
                auto& value = _value.template emplace<sequence_type>();
                ar <=> ::articuno::self(value, flags);
            } else {
                if (ar.is_null()) {
                    _value = nullptr;
                } else if (ar.is_signed_int()) {
                    ar <=> articuno::self(_value.template emplace<int64_t>());
                } else if (ar.is_unsigned_int()) {
                    ar <=> articuno::self(_value.template emplace<uint64_t>());
                } else if (ar.is_float()) {
                    ar <=> articuno::self(_value.template emplace<double_t>());
                } else if (ar.is_string()) {
                    ar <=> articuno::self(_value.template emplace<string_type>());
                }
            }
        }

        ::std::variant<nullptr_t, bool, int64_t, uint64_t, double_t, string_type,
                       sequence_type, map_type> _value{nullptr};

        friend class articuno::access;
    };
}
