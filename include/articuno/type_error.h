#pragma once

#include "serde_error.h"

namespace articuno {
    class type_error : public ::articuno::logic_serde_error {
    public:
        using ::articuno::logic_serde_error::serde_error;
    };
}
