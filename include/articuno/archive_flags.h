#pragma once

#include <cstdint>

#include <gluino/flags.h>

namespace articuno {
	gluino_flags(archive_flags, uint64_t,
		/**
		 * Disable object tracking on archives which support it.
		 */
		(disable_tracking, 1 << 0),

		/**
         * Disallow narrowing conversion on numbers when the value deserialized is outside the target type's
         * range.
         */
		(disallow_narrowing, 1 << 1),

		/**
         * Disallows rounding on numbers, such as reading floats as ints or rounding floats to nearest precise value.
         */
		(disallow_rounding, 1 << 2),

		/**
         * Enable support for raw pointers and references by default for all properties.
         */
		(allow_indirection, 1 << 3),

		/**
		 * Blocks resolving any nodes, which requires tracked, common anchors to be distinct, therefore causing failures
		 * when attempting to serialize shared objects with an archive which lacks object tracking support.
		 */
		(disallow_resolving, 1 << 4));
}