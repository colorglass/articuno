#pragma once

#include <gluino/concepts.h>

#include "archive_flags.h"
#include "value_flags.h"

namespace articuno {
	namespace serde {
		class value_flags_adl {
		public:
			constexpr explicit value_flags_adl(::articuno::value_flags flags) noexcept
				: _flags(flags) {
			}

			constexpr operator ::articuno::value_flags() const noexcept {
				return _flags;
			}

		private:
			::articuno::value_flags _flags;
		};

		template <::gluino::none Archive, ::gluino::none T>
		inline void serde(Archive&, T&, ::articuno::value_flags) {
			static_assert(::gluino::dependent_false<T>, "No matching non-intrusive serde handler was found.");
		}

		template <class Archive, class T>
		requires requires(Archive& ar, T& value, ::articuno::value_flags flags) {
			{ serde(ar, value, flags) };
		}
		inline void serde_adl(Archive& ar, T& value, ::articuno::value_flags flags) {
			serde(ar, value, flags);
		}
	}

	template <class Archive>
	concept archive = requires {
		typename Archive::serde_tag_type;
		typename Archive::value_type;
		typename Archive::traits_type;
		typename Archive::stream_type;
		typename Archive::archive_type;
		typename Archive::native_value_type;
		typename Archive::native_string_type;
		{ Archive::serde_tag } -> ::std::same_as<const typename Archive::serde_tag_type&>;
		{ Archive::flags } -> ::std::same_as<const ::articuno::archive_flags&>;
	} && (requires {
		{ Archive::serializing };
	} || requires {
		 { Archive::deserializing };
	});

	template <class Archive>
	concept serializing_archive = requires(Archive& ar) {
		{ Archive::serializing };
	} && ::articuno::archive<Archive>;

	template <class Archive>
	concept deserializing_archive = requires(Archive& ar) {
		{ Archive::deserializing };
	} && ::articuno::archive<Archive>;

	template <class Archive>
	concept serde_archive = serializing_archive<Archive> && deserializing_archive<Archive>;

	template <class T, class Archive>
	concept primitive = ::gluino::arithmetic<T> ||
						::std::same_as<T, typename Archive::native_string_type>;

	template <class T, class Archive>
	concept nonintrusive_serializable = requires(Archive& ar, ::gluino::unconst_t<T>& value, value_flags flags) {
		{ ::articuno::serde::serde_adl(ar, value, flags) };
	}
	&& ::articuno::serializing_archive<Archive>;

	template <class T, class Archive>
	concept nonintrusive_deserializable = requires(Archive& ar, T& value, value_flags flags) {
		{ ::articuno::serde::serde_adl(ar, value, flags) };
	}
	&& ::articuno::deserializing_archive<Archive>;

	template <class T, class Archive>
	concept intrusive_serializable = !::articuno::primitive<T, Archive> && !::articuno::nonintrusive_serializable<T, Archive>;

	template <class T, class Archive>
	concept intrusive_deserializable = !::articuno::primitive<T, Archive> && !::articuno::nonintrusive_deserializable<T, Archive>;

	template <class T, class Archive>
	concept custom_serializable = !::articuno::primitive<T, Archive> &&
	    (::articuno::intrusive_serializable<T, Archive> || ::articuno::nonintrusive_serializable<T, Archive>);

	template <class T, class Archive>
	concept custom_deserializable = !::articuno::primitive<T, Archive> &&
	    (::articuno::intrusive_deserializable<T, Archive> || ::articuno::nonintrusive_deserializable<T, Archive>);

	template <class Value>
	class self;

	template <class T, class Archive>
	concept serializable = ::articuno::serializing_archive<Archive> &&
	    (::articuno::primitive<T, Archive> || ::articuno::custom_serializable<T, Archive>);

	template <class T, class Archive>
	concept deserializable = ::articuno::deserializing_archive<Archive> &&
	    (::articuno::primitive<T, Archive> || ::articuno::custom_deserializable<T, Archive>);
}
