#pragma once

#include <gluino/string_cast.h>
#include <gluino/utility.h>
#include <libcuckoo/cuckoohash_map.hh>

#include "../../concepts.h"
#include "../../kv.h"
#include "../../next.h"
#include "../../object.h"
#include "../../sequence.h"
#include "../std/utility.h"

namespace articuno::serde {
	template <::articuno::serializing_archive Archive, class K, class V, class Hash, class Eq, class Alloc, ::std::size_t N>
	void serde(Archive& ar, const ::libcuckoo::cuckoohash_map<K, V, Hash, Eq, Alloc, N>& value, ::articuno::value_flags flags) {
        ar <=> ::articuno::sequence(flags);
		for (auto& entry : value) {
			ar <=> ::articuno::next(entry, flags);
		}
	}

	template <::articuno::deserializing_archive Archive, class K, class V, class Hash, class Eq, class Alloc, ::std::size_t N>
	void serde(Archive& ar, ::libcuckoo::cuckoohash_map<K, V, Hash, Eq, Alloc, N>& value, ::articuno::value_flags flags) {
		value.clear();
        if (!(ar <=> ::articuno::sequence(flags))) {
            return;
        }
		value.reserve(ar.size());
		for (::std::size_t i = 0; i < ar.size(); ++i) {
			std::pair<K, V> entry;
			ar <=> ::articuno::index(i, entry, flags);
			if constexpr (::std::move_constructible<decltype(entry)>) {
				value.emplace(::std::move(entry));
			} else if constexpr (::std::copy_constructible<decltype(entry)>) {
				value.emplace(entry);
			} else {
				static_assert(::gluino::dependent_false<T>, "Entries of flat_hash_map must be move or copy constructible.");
			}
		}
	}

	template <::articuno::serializing_archive Archive, ::gluino::any_string_type K, class V, class Hash, class Eq, class Alloc, ::std::size_t N>
	void serde(Archive& ar, const ::libcuckoo::cuckoohash_map<K, V, Hash, Eq, Alloc, N>& value, ::articuno::value_flags flags) {
        ar <=> ::articuno::object(flags);
		for (auto& entry : value) {
			ar <=> ::articuno::kv(entry.first, entry.second, flags);
		}
	}

	template <::articuno::deserializing_archive Archive, ::gluino::any_string_type K, class V, class Hash, class Eq, class Alloc, ::std::size_t N>
	void serde(Archive& ar, ::libcuckoo::cuckoohash_map<K, V, Hash, Eq, Alloc, N>& value, ::articuno::value_flags flags) {
		value.clear();
        if (!(ar <=> ::articuno::object(flags))) {
            return;
        }
		value.reserve(ar.size());
		for (auto& key : ar) {
			V val;
			ar <=> ::articuno::kv(key.first, key.second, val, flags);
			if constexpr (::std::move_constructible<V>) {
				static_assert(std::same_as<char, typename K::value_type>);
				value.emplace(::gluino::try_move(::gluino::string_cast<typename K::value_type>(key.second)),
					::std::move(val));
			} else if constexpr (::std::copy_constructible<V>) {
				value.emplace(::gluino::try_move(::gluino::string_cast<typename K::value_type>(key.second)),
					val);
			} else {
				static_assert(::gluino::dependent_false<T>, "Value of flat_hash_map must be move or copy constructible.");
			}
		}
	}
}
