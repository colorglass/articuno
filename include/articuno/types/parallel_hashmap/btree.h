#pragma once

#include <parallel_hashmap/btree.h>

#include <gluino/string_cast.h>
#include <gluino/utility.h>

#include "../../concepts.h"
#include "../../kv.h"
#include "../../next.h"
#include "../../object.h"
#include "../../sequence.h"
#include "../std/utility.h"

namespace articuno::serde {
    template <::articuno::serializing_archive Archive, class T, class Less, class Alloc>
    void serde(Archive& ar, const ::phmap::btree_set<T, Less, Alloc>& value, ::articuno::value_flags flags) {
        ar <=> ::articuno::sequence(flags);
        for (auto& entry : value) {
            ar <=> ::articuno::next(entry, flags);
        }
    }
    
    template <::articuno::deserializing_archive Archive, class T, class Less, class Alloc>
    void serde(Archive& ar, ::phmap::btree_set<T, Less, Alloc>& value, ::articuno::value_flags flags) {
        if (!(ar <=> ::articuno::sequence(flags))) {
            return;
        }
        for (::std::size_t i = 0; i < ar.size(); ++i) {
            T entry;
            ar <=> ::articuno::index(i, entry, flags);
            if constexpr (::std::move_constructible<decltype(entry)>) {
                value.emplace(::std::move(entry));
            } else if constexpr (::std::copy_constructible<decltype(entry)>) {
                value.emplace(entry);
            } else {
                static_assert(::gluino::dependent_false<T>, "Entries of btree_set must be move or copy constructible.");
            }
        }
    }

	template <::articuno::serializing_archive Archive, class K, class V, class Less, class Alloc>
	void serde(Archive& ar, const ::phmap::btree_map<K, V, Less, Alloc>& value, ::articuno::value_flags flags) {
        ar <=> ::articuno::sequence(flags);
		for (auto& entry : value) {
			ar <=> ::articuno::next(entry, flags);
		}
	}

	template <::articuno::deserializing_archive Archive, class K, class V, class Less, class Alloc>
	void serde(Archive& ar, ::phmap::btree_map<K, V, Less, Alloc>& value, ::articuno::value_flags flags) {
        if (!(ar <=> ::articuno::sequence(flags))) {
            return;
        }
		for (::std::size_t i = 0; i < ar.size(); ++i) {
			std::pair<K, V> entry;
			ar <=> ::articuno::index(i, entry, flags);
			if constexpr (::std::move_constructible<decltype(entry)>) {
				value.emplace(::std::move(entry));
			} else if constexpr (::std::copy_constructible<decltype(entry)>) {
				value.emplace(entry);
			} else {
				static_assert(::gluino::dependent_false<K>, "Entries of btree_map must be move or copy constructible.");
			}
		}
	}

	template <::articuno::serializing_archive Archive, ::gluino::any_string_type K, class V, class Less, class Alloc>
	void serde(Archive& ar, const ::phmap::btree_map<K, V, Less, Alloc>& value, ::articuno::value_flags flags) {
        ar <=> ::articuno::object(flags);
		for (auto& entry : value) {
			ar <=> ::articuno::kv(entry.second, entry.first, flags);
		}
	}

	template <::articuno::deserializing_archive Archive, ::gluino::any_string_type K, class V, class Less, class Alloc>
	void serde(Archive& ar, ::phmap::btree_map<K, V, Less, Alloc>& value, ::articuno::value_flags flags) {
        if (!(ar <=> ::articuno::object(flags))) {
            return;
        }
		for (auto& key : ar) {
			V val;
			ar <=> ::articuno::kv(val, key.first, key.second, flags);
			if constexpr (::std::move_constructible<V>) {
				static_assert(std::same_as<char, typename K::value_type>);
				value.emplace(::gluino::try_move(::gluino::string_cast<typename K::value_type>(key.second)),
					::std::move(val));
			} else if constexpr (::std::copy_constructible<V>) {
				value.emplace(::gluino::try_move(::gluino::string_cast<typename K::value_type>(key.second)),
					val);
			} else {
				static_assert(::gluino::dependent_false<K>, "Value of btree_map must be move or copy constructible.");
			}
		}
	}
}
