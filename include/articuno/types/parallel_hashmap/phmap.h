#pragma once

#include <gluino/string_cast.h>
#include <gluino/utility.h>
#include <parallel_hashmap/phmap.h>

#include "../../concepts.h"
#include "../../kv.h"
#include "../../next.h"
#include "../../object.h"
#include "../../sequence.h"
#include "../std/utility.h"

namespace articuno::serde {
    template <::articuno::serializing_archive Archive, class T, class Hash, class Eq, class Alloc>
    void serde(Archive& ar, const ::phmap::flat_hash_set<T, Hash, Eq, Alloc>& value, ::articuno::value_flags flags) {
        ar <=> ::articuno::sequence(flags);
        for (auto& entry : value) {
            ar <=> ::articuno::next(entry, flags);
        }
    }

    template <::articuno::deserializing_archive Archive, class T, class Hash, class Eq, class Alloc>
    void serde(Archive& ar, ::phmap::flat_hash_set<T, Hash, Eq, Alloc>& value, ::articuno::value_flags flags) {
        value.clear();
        if (!(ar <=> ::articuno::sequence(flags))) {
            return;
        }
        value.reserve(ar.size());
        for (::std::size_t i = 0; i < ar.size(); ++i) {
            T entry;
            ar <=> ::articuno::index(i, entry, flags);
            if constexpr (::std::move_constructible<decltype(entry)>) {
                value.emplace(::std::move(entry));
            } else if constexpr (::std::copy_constructible<decltype(entry)>) {
                value.emplace(entry);
            } else {
                static_assert(::gluino::dependent_false<T>,
                    "Entries of flat_hash_set must be move or copy constructible.");
            }
        }
    }

    template <::articuno::serializing_archive Archive, class T, class Hash, class Eq, class Alloc>
    void serde(Archive& ar, const ::phmap::node_hash_set<T, Hash, Eq, Alloc>& value, ::articuno::value_flags flags) {
        ar <=> ::articuno::sequence(flags);
        for (auto& entry : value) {
            ar <=> ::articuno::next(entry, flags);
        }
    }

    template <::articuno::deserializing_archive Archive, class T, class Hash, class Eq, class Alloc>
    void serde(Archive& ar, ::phmap::node_hash_set<T, Hash, Eq, Alloc>& value, ::articuno::value_flags flags) {
        value.clear();
        if (!(ar <=> ::articuno::sequence(flags))) {
            return;
        }
        value.reserve(ar.size());
        for (::std::size_t i = 0; i < ar.size(); ++i) {
            T entry;
            ar <=> ::articuno::index(i, entry, flags);
            if constexpr (::std::move_constructible<decltype(entry)>) {
                value.emplace(::std::move(entry));
            } else if constexpr (::std::copy_constructible<decltype(entry)>) {
                value.emplace(entry);
            } else {
                static_assert(::gluino::dependent_false<T>,
                    "Entries of node_hash_set must be move or copy constructible.");
            }
        }
    }

    template <::articuno::serializing_archive Archive, class T, class Hash, class Eq, class Alloc, ::std::size_t N,
              class Mutex>
    void serde(Archive& ar, const ::phmap::parallel_flat_hash_set<T, Hash, Eq, Alloc, N, Mutex>& value,
               ::articuno::value_flags flags) {
        ar <=> ::articuno::sequence(flags);
        for (auto& entry : value) {
            ar <=> ::articuno::next(entry, flags);
        }
    }

    template <::articuno::deserializing_archive Archive, class T, class Hash, class Eq, class Alloc, ::std::size_t N,
              class Mutex>
    void serde(Archive& ar, ::phmap::parallel_flat_hash_set<T, Hash, Eq, Alloc, N, Mutex>& value,
               ::articuno::value_flags flags) {
        value.clear();
        if (!(ar <=> ::articuno::sequence(flags))) {
            return;
        }
        value.reserve(ar.size());
        for (::std::size_t i = 0; i < ar.size(); ++i) {
            T entry;
            ar <=> ::articuno::index(i, entry, flags);
            if constexpr (::std::move_constructible<decltype(entry)>) {
                value.emplace(::std::move(entry));
            } else if constexpr (::std::copy_constructible<decltype(entry)>) {
                value.emplace(entry);
            } else {
                static_assert(::gluino::dependent_false<T>,
                    "Entries of parallel_flat_hash_set must be move or copy constructible.");
            }
        }
    }

    template <::articuno::serializing_archive Archive, class T, class Hash, class Eq, class Alloc, ::std::size_t N,
              class Mutex>
    void serde(Archive& ar, const ::phmap::parallel_node_hash_set<T, Hash, Eq, Alloc, N, Mutex>& value,
               ::articuno::value_flags flags) {
        ar <=> ::articuno::object(flags);
        for (auto& entry : value) {
            ar <=> ::articuno::next(entry, flags);
        }
    }

    template <::articuno::deserializing_archive Archive, class T, class Hash, class Eq, class Alloc, ::std::size_t N,
              class Mutex>
    void serde(Archive& ar, ::phmap::parallel_node_hash_set<T, Hash, Eq, Alloc, N, Mutex>& value,
               ::articuno::value_flags flags) {
        value.clear();
        if (!(ar <=> ::articuno::sequence(flags))) {
            return;
        }
        value.reserve(ar.size());
        for (::std::size_t i = 0; i < ar.size(); ++i) {
            T entry;
            ar <=> ::articuno::index(i, entry, flags);
            if constexpr (::std::move_constructible<decltype(entry)>) {
                value.emplace(::std::move(entry));
            } else if constexpr (::std::copy_constructible<decltype(entry)>) {
                value.emplace(entry);
            } else {
                static_assert(::gluino::dependent_false<T>,
                    "Entries of parallel_node_hash_set must be move or copy constructible.");
            }
        }
    }

    template <::articuno::serializing_archive Archive, class K, class V, class Hash, class Eq, class Alloc>
    void serde(Archive& ar, const ::phmap::flat_hash_map<K, V, Hash, Eq, Alloc>& value, ::articuno::value_flags flags) {
        ar <=> ::articuno::sequence(flags);
        for (auto& entry : value) {
            ar <=> ::articuno::next(entry, flags);
        }
    }

    template <::articuno::deserializing_archive Archive, class K, class V, class Hash, class Eq, class Alloc>
    void serde(Archive& ar, ::phmap::flat_hash_map<K, V, Hash, Eq, Alloc>& value, ::articuno::value_flags flags) {
        value.clear();
        if (!(ar <=> ::articuno::sequence(flags))) {
            return;
        }
        value.reserve(ar.size());
        for (::std::size_t i = 0; i < ar.size(); ++i) {
            std::pair<K, V> entry;
            ar <=> ::articuno::index(i, entry, flags);
            if constexpr (::std::move_constructible<decltype(entry)>) {
                value.emplace(::std::move(entry));
            } else if constexpr (::std::copy_constructible<decltype(entry)>) {
                value.emplace(entry);
            } else {
                static_assert(::gluino::dependent_false<K>,
                    "Entries of flat_hash_map must be move or copy constructible.");
            }
        }
    }

    template <::articuno::serializing_archive Archive, ::gluino::any_string_type K, class V, class Hash, class Eq,
              class Alloc>
    void serde(Archive& ar, const ::phmap::flat_hash_map<K, V, Hash, Eq, Alloc>& value, ::articuno::value_flags flags) {
        ar <=> ::articuno::object(flags);
        for (auto& entry : value) {
            ar <=> ::articuno::kv(entry.second, entry.first, flags);
        }
    }

    template <::articuno::deserializing_archive Archive, ::gluino::any_string_type K, class V, class Hash, class Eq,
              class Alloc>
    void serde(Archive& ar, ::phmap::flat_hash_map<K, V, Hash, Eq, Alloc>& value, ::articuno::value_flags flags) {
        value.clear();
        if (!(ar <=> ::articuno::object(flags))) {
            return;
        }
        value.reserve(ar.size());
        for (auto& key : ar) {
            V val;
            ar <=> ::articuno::kv(val, key.first, key.second, flags);
            if constexpr (::std::move_constructible<V>) {
                value.emplace(::gluino::try_move(::gluino::string_cast<typename K::value_type>(key.second)),
                              ::std::move(val));
            } else if constexpr (::std::copy_constructible<V>) {
                value.emplace(::gluino::try_move(::gluino::string_cast<typename K::value_type>(key.second)), val);
            } else {
                static_assert(::gluino::dependent_false<K>,
                    "Value of flat_hash_map must be move or copy constructible.");
            }
        }
    }

    template <::articuno::serializing_archive Archive, class K, class V, class Hash, class Eq, class Alloc>
    void serde(Archive& ar, const ::phmap::node_hash_map<K, V, Hash, Eq, Alloc>& value, ::articuno::value_flags flags) {
        ar <=> ::articuno::sequence(flags);
        for (auto& entry : value) {
            ar <=> ::articuno::next(entry, flags);
        }
    }

    template <::articuno::deserializing_archive Archive, class K, class V, class Hash, class Eq, class Alloc>
    void serde(Archive& ar, ::phmap::node_hash_map<K, V, Hash, Eq, Alloc>& value, ::articuno::value_flags flags) {
        value.clear();
        if (!(ar <=> ::articuno::sequence(flags))) {
            return;
        }
        value.reserve(ar.size());
        for (::std::size_t i = 0; i < ar.size(); ++i) {
            std::pair<K, V> entry;
            ar <=> ::articuno::index(i, entry, flags);
            if constexpr (::std::move_constructible<decltype(entry)>) {
                value.emplace(::std::move(entry));
            } else if constexpr (::std::copy_constructible<decltype(entry)>) {
                value.emplace(entry);
            } else {
                static_assert(::gluino::dependent_false<K>,
                    "Entries of node_hash_map must be move or copy constructible.");
            }
        }
    }

    template <::articuno::serializing_archive Archive, ::gluino::any_string_type K, class V, class Hash, class Eq,
              class Alloc>
    void serde(Archive& ar, const ::phmap::node_hash_map<K, V, Hash, Eq, Alloc>& value, ::articuno::value_flags flags) {
        ar <=> ::articuno::object(flags);
        for (auto& entry : value) {
            ar <=> ::articuno::kv(entry.second, entry.first, flags);
        }
    }

    template <::articuno::deserializing_archive Archive, ::gluino::any_string_type K, class V, class Hash, class Eq,
              class Alloc>
    void serde(Archive& ar, ::phmap::node_hash_map<K, V, Hash, Eq, Alloc>& value, ::articuno::value_flags flags) {
        value.clear();
        if (!(ar <=> ::articuno::object(flags))) {
            return;
        }
        value.reserve(ar.size());
        for (auto& key : ar) {
            V val;
            ar <=> ::articuno::kv(val, key.first, key.second, flags);
            if constexpr (::std::move_constructible<V>) {
                value.emplace(::gluino::try_move(::gluino::string_cast<typename K::value_type>(key.second)),
                              ::std::move(val));
            } else if constexpr (::std::copy_constructible<V>) {
                value.emplace(::gluino::try_move(::gluino::string_cast<typename K::value_type>(key.second)), val);
            } else {
                static_assert(::gluino::dependent_false<K>,
                    "Value of node_hash_map must be move or copy constructible.");
            }
        }
    }

    template <::articuno::serializing_archive Archive, class K, class V, class Hash, class Eq, class Alloc,
              ::std::size_t N, class Mutex>
    void serde(Archive& ar, const ::phmap::parallel_flat_hash_map<K, V, Hash, Eq, Alloc, N, Mutex>& value,
               ::articuno::value_flags flags) {
        ar <=> ::articuno::sequence(flags);
        for (auto& entry : value) {
            ar <=> ::articuno::next(entry, flags);
        }
    }

    template <::articuno::deserializing_archive Archive, class K, class V, class Hash, class Eq, class Alloc,
              ::std::size_t N, class Mutex>
    void serde(Archive& ar, ::phmap::parallel_flat_hash_map<K, V, Hash, Eq, Alloc, N, Mutex>& value,
               ::articuno::value_flags flags) {
        value.clear();
        if (!(ar <=> ::articuno::sequence(flags))) {
            return;
        }
        value.reserve(ar.size());
        for (::std::size_t i = 0; i < ar.size(); ++i) {
            std::pair<K, V> entry;
            ar <=> ::articuno::index(i, entry, flags);
            if constexpr (::std::move_constructible<decltype(entry)>) {
                value.emplace(::std::move(entry));
            } else if constexpr (::std::copy_constructible<decltype(entry)>) {
                value.emplace(entry);
            } else {
                static_assert(::gluino::dependent_false<K>,
                    "Entries of parallel_flat_hash_map must be move or copy constructible.");
            }
        }
    }

    template <::articuno::serializing_archive Archive, ::gluino::any_string_type K, class V, class Hash, class Eq,
              class Alloc, ::std::size_t N, class Mutex>
    void serde(Archive& ar, const ::phmap::parallel_flat_hash_map<K, V, Hash, Eq, Alloc, N, Mutex>& value,
               ::articuno::value_flags flags) {
        ar <=> ::articuno::object(flags);
        for (auto& entry : value) {
            ar <=> ::articuno::kv(entry.second, entry.first, flags);
        }
    }

    template <::articuno::deserializing_archive Archive, ::gluino::any_string_type K, class V, class Hash, class Eq,
              class Alloc, ::std::size_t N, class Mutex>
    void serde(Archive& ar, ::phmap::parallel_flat_hash_map<K, V, Hash, Eq, Alloc, N, Mutex>& value,
               ::articuno::value_flags flags) {
        value.clear();
        if (!(ar <=> ::articuno::object(flags))) {
            return;
        }
        value.reserve(ar.size());
        for (auto& key : ar) {
            V val;
            ar <=> ::articuno::kv(val, key.first, key.second, flags);
            if constexpr (::std::move_constructible<V>) {
                value.emplace(::gluino::try_move(::gluino::string_cast<typename K::value_type>(key.second)),
                              ::std::move(val));
            } else if constexpr (::std::copy_constructible<V>) {
                value.emplace(::gluino::try_move(::gluino::string_cast<typename K::value_type>(key.second)), val);
            } else {
                static_assert(::gluino::dependent_false<K>,
                    "Value of parallel_flat_hash_map must be move or copy constructible.");
            }
        }
    }

    template <::articuno::serializing_archive Archive, class K, class V, class Hash, class Eq, class Alloc,
              ::std::size_t N, class Mutex>
    void serde(Archive& ar, const ::phmap::parallel_node_hash_map<K, V, Hash, Eq, Alloc, N, Mutex>& value,
               ::articuno::value_flags flags) {
        ar <=> ::articuno::sequence(flags);
        for (auto& entry : value) {
            ar <=> ::articuno::next(entry, flags);
        }
    }

    template <::articuno::deserializing_archive Archive, class K, class V, class Hash, class Eq, class Alloc,
              ::std::size_t N, class Mutex>
    void serde(Archive& ar, ::phmap::parallel_node_hash_map<K, V, Hash, Eq, Alloc, N, Mutex>& value,
               ::articuno::value_flags flags) {
        value.clear();
        if (!(ar <=> ::articuno::sequence(flags))) {
            return;
        }
        value.reserve(ar.size());
        for (::std::size_t i = 0; i < ar.size(); ++i) {
            std::pair<K, V> entry;
            ar <=> ::articuno::index(i, entry, flags);
            if constexpr (::std::move_constructible<decltype(entry)>) {
                value.emplace(::std::move(entry));
            } else if constexpr (::std::copy_constructible<decltype(entry)>) {
                value.emplace(entry);
            } else {
                static_assert(::gluino::dependent_false<K>,
                    "Entries of parallel_node_hash_map must be move or copy constructible.");
            }
        }
    }

    template <::articuno::serializing_archive Archive, ::gluino::any_string_type K, class V, class Hash, class Eq,
              class Alloc, ::std::size_t N, class Mutex>
    void serde(Archive& ar, const ::phmap::parallel_node_hash_map<K, V, Hash, Eq, Alloc, N, Mutex>& value,
               ::articuno::value_flags flags) {
        ar <=> ::articuno::object(flags);
        for (auto& entry : value) {
            ar <=> ::articuno::kv(entry.second, entry.first, flags);
        }
    }

    template <::articuno::deserializing_archive Archive, ::gluino::any_string_type K, class V, class Hash, class Eq,
              class Alloc, ::std::size_t N, class Mutex>
    void serde(Archive& ar, ::phmap::parallel_node_hash_map<K, V, Hash, Eq, Alloc, N, Mutex>& value,
               ::articuno::value_flags flags) {
        value.clear();
        if (!(ar <=> ::articuno::object(flags))) {
            return;
        }
        value.reserve(ar.size());
        for (auto& key : ar) {
            V val;
            ar <=> ::articuno::kv(val, key.first, key.second, flags);
            if constexpr (::std::move_constructible<V>) {
                value.emplace(::gluino::try_move(::gluino::string_cast<typename K::value_type>(key.second)),
                              ::std::move(val));
            } else if constexpr (::std::copy_constructible<V>) {
                value.emplace(::gluino::try_move(::gluino::string_cast<typename K::value_type>(key.second)), val);
            } else {
                static_assert(::gluino::dependent_false<K>,
                    "Value of parallel_node_hash_map must be move or copy constructible.");
            }
        }
    }
}  // namespace articuno::serde
