#pragma once

#include <gluino/random_engine.h>

#include "../std/iostream.h"

namespace articuno::serde {
	template <::articuno::serializing_archive Archive, class Engine, class Mutex>
	void serde(Archive& ar, const ::gluino::random_engine<Engine, Mutex>& value, ::articuno::value_flags flags) {
		::std::stringstream out;
		out << value;
		::std::string str = out.str();
		ar <=> ::articuno::self(str, flags);
	}

	template <::articuno::deserializing_archive Archive, class Engine, class Mutex>
	void serde(Archive& archive, ::gluino::random_engine<Engine, Mutex>& value, ::articuno::value_flags flags) {
		::std::stringstream in;
		archive <=> ::articuno::self(in, flags);
		in >> value;
	}
}
