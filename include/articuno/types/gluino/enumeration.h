#pragma once

#include <gluino/enumeration.h>

#include "../../concepts.h"
#include "../../self.h"

namespace articuno::serde {
	template <::articuno::serializing_archive Archive, class Enum>
	requires (::gluino::is_enumeration_v<Enum>)
	void serde(Archive& ar, const Enum& value, ::articuno::value_flags flags) {
		auto name = value.name();
		ar <=> ::articuno::self(name, flags);
	}

	template <::articuno::deserializing_archive Archive, class Enum>
	requires (::gluino::is_enumeration_v<Enum>)
	void serde(Archive& ar, Enum& value, ::articuno::value_flags flags) {
		std::string name;
		ar <=> ::articuno::self(name, flags);
		value = Enum(name);
	}
}
