#pragma once

#include <gluino/ptr.h>

#include "../std/pointer.h"

namespace articuno::serde {
	template <::articuno::archive Archive, class T>
	void serde(Archive& ar, ::gluino::raw_ptr<T>& value, ::articuno::value_flags flags) {
		ar <=> ::articuno::self(reinterpret_cast<T*&>(value), flags);
	}

	template <::articuno::archive Archive, class T>
	void serde(Archive& ar, ::gluino::optional_ptr<T>& value, ::articuno::value_flags flags) {
		ar <=> ::articuno::self(reinterpret_cast<T*&>(value), flags);
	}

	template <::articuno::archive Archive, class T>
	void serde(Archive& ar, ::gluino::safe_ptr<T>& value, ::articuno::value_flags flags) {
		ar <=> ::articuno::self(reinterpret_cast<T*&>(value), flags);
	}
}
