#pragma once

#include <gluino/flags.h>

#include "../../concepts.h"
#include "../../next.h"

namespace articuno::serde {
	template <::articuno::serializing_archive Archive, class Flags>
	requires (::gluino::flags<Flags>)
	void serde(Archive& ar, const Flags& value, ::articuno::value_flags flags) {
		for (auto val : Flags::values()) {
			if (value.has(val)) {
				auto name = val.names()[0];
				ar <=> ::articuno::next(name, flags);
			}
		}
	}

	template <::articuno::deserializing_archive Archive, class Flags>
	requires (::gluino::flags<Flags>)
	void serde(Archive& ar, Flags& value, ::articuno::value_flags flags) {
		value.reset();
		for (std::size_t i = 0; i < ar.size(); ++i) {
			std::string name;
			ar <=> ::articuno::next(name, flags);
			value.set(Flags(name));
		}
	}
}
