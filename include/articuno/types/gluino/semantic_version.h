#pragma once

#include <gluino/semantic_version.h>

#include "../../concepts.h"
#include "../../self.h"

namespace articuno::serde {
	template <::articuno::serializing_archive Archive>
	void serde(Archive& ar, const ::gluino::semantic_version& value, ::articuno::value_flags flags) {
		::std::string str = ::std::to_string(value);
		ar <=> ::articuno::self(str, flags);
	}

	template <::articuno::deserializing_archive Archive>
	void serde(Archive& archive, ::gluino::semantic_version& value, ::articuno::value_flags flags) {
		::std::string str;
		archive <=> ::articuno::self(str, flags);
		value = ::gluino::semantic_version(str);
	}
}
