#pragma once

#include <boost/variant.hpp>

#include "../../concepts.h"
#include "../../kv.h"

namespace articuno::serde {
	namespace detail {
		template <::articuno::serializing_archive Archive>
		struct boost_variant_visitor : public ::boost::static_visitor<int> {
			inline explicit boost_variant_visitor(Archive& archive, ::articuno::value_flags flags) noexcept
				: _archive(archive), _flags(flags) {
			}

			int operator()(auto&& arg) {
				_archive <=> ::articuno::kv(arg, "value", _flags);
			}

		private:
			Archive& _archive;
			::articuno::value_flags _flags;
		};
	}

	template <::articuno::serializing_archive Archive, class... Ts>
	void serde(Archive& ar, const ::boost::variant<Ts...>& value, ::articuno::value_flags flags) {
		ar <=> ::articuno::kv(value.index(), "type", ar.value_flags());
		::boost::apply_visitor(::articuno::serde::detail::boost_variant_visitor(ar, flags), value);
	}

	template <::articuno::deserializing_archive Archive, class... Ts>
	void serde(Archive& ar, ::boost::variant<Ts...>& value, ::articuno::value_flags flags) {
		::std::size_t index;
		ar <=> ::articuno::kv(index, "type", flags + ::articuno::value_flags::required);
		load<Archive, 0>(ar, value, index);
	}
}
