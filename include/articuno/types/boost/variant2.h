#pragma once

#include <boost/variant2.hpp>

#include "../../concepts.h"
#include "../../kv.h"

namespace articuno::serde {
	namespace detail {
		template <::articuno::deserializing_archive Archive, ::std::size_t Index, class... Ts>
		static void load([[maybe_unused]] Archive& archive, [[maybe_unused]] ::boost::variant2::variant<Ts...>& value,
			[[maybe_unused]] ::articuno::value_flags flags, [[maybe_unused]] ::std::size_t index) {
			if constexpr (Index < sizeof...(Ts)) {
				if (Index == index) {
					auto& element = value.template emplace<Index>();
					archive <=> ::articuno::kv(element, "value",
									 flags + ::articuno::value_flags::required);
					return;
				}
				load<Archive, Index + 1>(archive, value, index);
			}
		}
	}

	template <::articuno::serializing_archive Archive, class... Ts>
	void serde(Archive& ar, const ::boost::variant2::variant<Ts...>& value, ::articuno::value_flags flags) {
		ar <=> ::articuno::kv(value.index(), "type", ar.value_flags());
		::boost::variant2::visit([&](auto&& arg) {
			ar <=> ::articuno::kv(arg, "value", ar.value_flags());
		}, value);
	}

	template <::articuno::deserializing_archive Archive, class... Ts>
	void serde(Archive& ar, ::boost::variant2::variant<Ts...>& value, ::articuno::value_flags flags) {
		::std::size_t index;
		ar <=> ::articuno::kv(index, "type", flags + ::articuno::value_flags::required);
		load<Archive, 0>(ar, value, index);
	}
}
