#pragma once

#include <boost/optional.hpp>

#include "../../concepts.h"
#include "../../format_error.h"
#include "../../null.h"
#include "../../self.h"

namespace articuno::serde {
	template <::articuno::serializing_archive Archive, class T>
	requires (::articuno::serializable<T, Archive>)
		void serde(Archive& ar, const ::boost::optional<T>& value, ::articuno::value_flags flags) {
		if (value.has_value()) {
			const auto& val = value.value();
			ar <=> ::articuno::self(val, flags + ::articuno::value_flags::nullable);
		} else {
			if (flags.has(::articuno::value_flags::required)) {
				throw ::articuno::format_error("Required field was not set; std::optional was left empty.");
			} else if (flags.has(::articuno::value_flags::nullable)) {
				ar <=> ::articuno::null(flags);
			}
		}
	}

	template <::articuno::deserializing_archive Archive, class T>
	requires (::articuno::deserializable<T, Archive>)
		void serde(Archive& ar, ::boost::optional<T>& value, ::articuno::value_flags flags) {
		T val;
		if (!(ar <=> ::articuno::self(val, flags + ::articuno::value_flags::nullable)) &&
			flags.has(::articuno::value_flags::required)) {
			value.reset();
			throw ::articuno::format_error("Required field was not set; std::optional would have been left empty.");
		}
		if constexpr (::std::move_constructible<T>) {
			value.emplace(::std::move(val));
		} else if constexpr (::std::copy_constructible<T>) {
			value.emplace(val);
		} else {
			static_assert(::gluino::dependent_false<T>,
			    "Optional type cannot be deserialized, it's value type must move or copy constructible.");
		}
	}
}
