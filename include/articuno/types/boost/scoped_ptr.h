#pragma once

#include <boost/scoped_ptr.hpp>

#include "../std/pointer.h"

namespace articuno::serde {
	template <::articuno::serializing_archive Archive, class T>
	requires (::articuno::serializable<T, Archive>)
		void serde(Archive& ar, const ::boost::scoped_ptr<T>& value, ::articuno::value_flags flags) {
		ar <=> ::articuno::self(*value, flags + ::articuno::value_flags::disable_tracking);
	}

	template <::articuno::deserializing_archive Archive, class T>
	requires (::articuno::deserializable<T, Archive>)
		void serde(Archive& ar, ::boost::scoped_ptr<T>& value, ::articuno::value_flags flags) {
		T* ptr;
		ar <=> ::articuno::self(ptr, flags + ::articuno::value_flags::disable_tracking);
		value.reset(ptr);
	}
}
