#pragma once

#include "optional.h"
#include "scoped_array.h"
#include "scoped_ptr.h"
#include "shared_array.h"
#include "shared_ptr.h"
#include "variant.h"
#include "variant2.h"
#include "weak_ptr.h"
