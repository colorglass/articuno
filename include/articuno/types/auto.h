#include "gluino/gluino.h"
#include "parallel_hashmap/parallel_hashmap.h"
#include "std/std.h"

#ifdef BOOST_OPTIONAL_OPTIONAL_FLC_19NOV2002_HPP
#include "boost/optional.h"
#endif

#ifdef BOOST_SMART_PTR_SCOPED_PTR_HPP_INCLUDED
#include "boost/scoped_ptr.h"
#endif

#ifdef _CUCKOOHASH_MAP_HH
#include "libcuckoo/cuckoohash_map.h"
#endif

#ifdef ROBIN_HOOD_H_INCLUDED
#include "robin_hood/robin_hood.h"
#endif
