#pragma once

#include <source_location>

#include "zstring.h"
#include "../../kv.h"

namespace articuno::serde {
	template <::articuno::serializing_archive Archive>
	void serde(Archive& ar, ::std::source_location& value, ::articuno::value_flags flags) {
		auto line = value.line();
		auto column = value.column();
		const auto* file_name = value.file_name();
		const auto* function_name = value.function_name();
		ar <=> ::articuno::kv(line, "line", flags);
		ar <=> ::articuno::kv(column, "column", flags);
		ar <=> ::articuno::kv(file_name, "filename", flags);
		ar <=> ::articuno::kv(function_name, "function", flags);
	}
}
