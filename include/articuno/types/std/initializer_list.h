#pragma once

#include <initializer_list>

#include "../../concepts.h"

namespace articuno::serde {
	template <::articuno::serializing_archive Archive, class T>
	void serde(Archive& ar, const ::std::initializer_list<T>& value, ::articuno::value_flags flags) {
        if (ar <=> ::articuno::sequence(flags)) {
            for (auto& entry : value) {
                ar <=> ::articuno::next(value, flags);
            }
        }
	}
}
