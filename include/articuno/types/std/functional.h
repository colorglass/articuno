#pragma once

#include <functional>

#include "pointer.h"

namespace articuno::serde {
	template <::articuno::archive Archive, class T>
	void serde(Archive& ar, ::std::reference_wrapper<T>& value, ::articuno::value_flags flags) {
		ar <=> ::articuno::self(reinterpret_cast<T*&>(value), flags);
	}
}
