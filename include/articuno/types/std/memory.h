#pragma once

#include <memory>

#include "pointer.h"

namespace articuno::serde {
	template <::articuno::serializing_archive Archive, class T>
	requires (::articuno::serializable<T, Archive>)
		void serde(Archive& ar, const ::std::unique_ptr<T>& value, ::articuno::value_flags flags) {
		ar <=> ::articuno::self(*value, flags + ::articuno::value_flags::disable_tracking);
	}

	template <::articuno::deserializing_archive Archive, class T>
	requires (::articuno::deserializable<T, Archive>)
		void serde(Archive& ar, ::std::unique_ptr<T>& value, ::articuno::value_flags flags) {
		ar <=> ::articuno::self(reinterpret_cast<T*&>(value), flags + ::articuno::value_flags::disable_tracking);
	}

	// TODO: Handle shared usage of shared_ptr.
	template <::articuno::serializing_archive Archive, class T>
	requires (::articuno::serializable<T, Archive>)
		void serde(Archive& ar, const ::std::shared_ptr<T>& value, ::articuno::value_flags flags) {
		ar <=> ::articuno::self(*value, flags);
	}

	template <::articuno::deserializing_archive Archive, class T>
	requires (::articuno::deserializable<T, Archive>)
		void serde(Archive& ar, ::std::shared_ptr<T>& value, ::articuno::value_flags flags) {
		T* ptr{nullptr};
		ar <=> ::articuno::self(ptr, flags);
		value.reset(ptr);
	}
}
