#pragma once

#include <iostream>

#include "../../concepts.h"
#include "../../self.h"

namespace articuno::serde {
	template <::articuno::serializing_archive Archive, class T>
	requires (::gluino::subtype_of<T, ::std::basic_istream<typename T::char_type, typename T::traits_type>>)
	void serde(Archive& ar, T& value, ::articuno::value_flags flags) {
		::std::basic_string<typename T::char_type, typename T::traits_type> str;
		while (!value.eof()) {
			char buffer[4096];
			value.read(buffer, sizeof(buffer));
			str.append(buffer, value.gcount());
		}
		ar <=> ::articuno::self(str, flags);
	}

	template <::articuno::deserializing_archive Archive, class T>
	requires (::gluino::subtype_of<T, ::std::basic_ostream<typename T::char_type, typename T::traits_type>>)
	void serde(Archive& ar, T& value, ::articuno::value_flags flags) {
		::std::basic_string<typename T::char_type, typename T::traits_type> str;
		ar <=> ::articuno::self(str, flags);
		value.write(str.data(), str.size());
	}
}
