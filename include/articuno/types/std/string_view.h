#pragma once

#include <string>

#include <gluino/string_cast.h>

#include "../../concepts.h"
#include "../../self.h"

namespace articuno::serde {
	template <::articuno::serializing_archive Archive, class Char, class Traits>
	requires (::gluino::string_view<typename Archive::native_string_type>)
	void serde(Archive& ar, const ::std::basic_string_view<Char, Traits>& value, ::articuno::value_flags flags) {
		if constexpr (sizeof(Char) == sizeof(typename Archive::native_value_type)) {
			ar <=> ::articuno::self(reinterpret_cast<::std::basic_string_view<typename Archive::native_value_type>&>(value), flags);
		} else {
			decltype(auto) str = ::gluino::string_cast<typename Archive::native_value_type>(value);
			ar <=> ::articuno::self(str, flags);
		}
	}

	template <::articuno::serializing_archive Archive, class Char, class Traits>
	requires (::gluino::cstring<typename Archive::native_string_type>)
	void serde(Archive& ar, const ::std::basic_string_view<Char, Traits>& value, ::articuno::value_flags flags) {
		if constexpr (sizeof(Char) == sizeof(typename Archive::native_value_type)) {
			const auto* data = value.data();
			ar <=> ::articuno::self(reinterpret_cast<const Char*>(data), flags);
		} else {
			decltype(auto) str = ::gluino::string_cast<typename Archive::native_value_type>(value);
			const auto* data = value.data();
			ar <=> ::articuno::self(data, flags);
		}
	}

	template <::articuno::serializing_archive Archive, class Char, class Traits>
	requires (::gluino::string<typename Archive::native_string_type>)
	void serde(Archive& ar, const ::std::basic_string_view<Char, Traits>& value, ::articuno::value_flags flags) {
		::std::basic_string<Char, Traits> str = value.data();
		ar <=> ::articuno::self(str, flags);
	}
}
