#pragma once

#include <string>

#include <gluino/string_cast.h>

#include "../../concepts.h"
#include "../../self.h"

namespace articuno::serde {
	template <::articuno::archive Archive, class Char, class Traits, class Alloc>
	requires (::gluino::string<typename Archive::native_string_type>)
	void serde(Archive& ar, ::std::basic_string<Char, Traits, Alloc>& value, ::articuno::value_flags flags) {
		if constexpr (sizeof(Char) == sizeof(typename Archive::native_value_type)) {
			ar <=> ::articuno::self(reinterpret_cast<::std::basic_string<typename Archive::native_value_type>&>(value), flags);
		} else {
			decltype(auto) str = ::gluino::string_cast<typename Archive::native_value_type>(value);
			ar <=> ::articuno::self(str, flags);
		}
	}

	template <::articuno::serializing_archive Archive, class Char, class Traits, class Alloc>
	requires (::gluino::string_view<typename Archive::native_string_type>)
	void serde(Archive& ar, ::std::basic_string<Char, Traits, Alloc>& value, ::articuno::value_flags flags) {
		if constexpr (sizeof(Char) == sizeof(typename Archive::native_value_type)) {
			::std::basic_string_view<typename Archive::native_value_type> view(
				reinterpret_cast<::std::basic_string<typename Archive::native_value_type>&>(value));
			ar <=> ::articuno::self(view, flags);
		} else {
			decltype(auto) str = ::gluino::string_cast<typename Archive::native_value_type>(value);
			ar <=> ::articuno::self(str, flags);
		}
	}

	template <::articuno::serializing_archive Archive, class Char, class Traits, class Alloc>
	requires (::gluino::cstring<typename Archive::native_string_type>)
		void serde(Archive& ar, ::std::basic_string<Char, Traits, Alloc>& value, ::articuno::value_flags flags) {
		if constexpr (sizeof(Char) == sizeof(typename Archive::native_value_type)) {
			auto* str = reinterpret_cast<::std::basic_string<typename Archive::native_value_type>&>(value).c_str();
			ar <=> ::articuno::self(str, flags);
		} else {
			decltype(auto) str = ::gluino::string_cast<typename Archive::native_value_type>(value);
			ar <=> ::articuno::self(str.data(), flags);
		}
	}

	template <::articuno::deserializing_archive Archive, class Char, class Traits, class Alloc>
	requires (::gluino::cstring<typename Archive::native_string_type>)
		void serde(Archive& ar, ::std::basic_string<Char, Traits, Alloc>& value, ::articuno::value_flags flags) {
		const typename Archive::native_value_type* native;
		ar <=> ::articuno::self(native, flags);
		if constexpr (sizeof(Char) == sizeof(typename Archive::native_value_type)) {
			::std::basic_string<typename Archive::native_value_type> str(native);
			value = ::std::move(reinterpret_cast<::std::basic_string<Char, Traits, Alloc>&>(str));
		} else {
			decltype(auto) str = ::gluino::string_cast<typename Archive::Char>(value);
			value = ::gluino::try_move(reinterpret_cast<::std::basic_string<Char, Traits, Alloc>&>(str));
		}
	}
}
