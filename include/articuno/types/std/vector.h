#pragma once

#include <vector>

#include "../../concepts.h"
#include "../../next.h"
#include "../../sequence.h"

namespace articuno::serde {
    template <::articuno::archive Archive, class T, class Alloc>
    void serde(Archive& ar, ::std::vector<T, Alloc>& value,
               ::articuno::value_flags flags) {
        ar <=> ::articuno::sequence(flags);
        if constexpr (::articuno::deserializing_archive<Archive>) {
            value.clear();
            value.resize(ar.size());
        }
        for (auto& entry : value) {
            ar <=> ::articuno::next(entry, flags);
        }
    }

    template <::articuno::archive Archive, class Alloc>
    void serde(Archive& ar, ::std::vector<bool, Alloc>& value,
               ::articuno::value_flags flags) {
        ar <=> ::articuno::sequence(flags);
        if constexpr (::articuno::deserializing_archive<Archive>) {
            value.resize(ar.size());
        }
        for (auto entry : value) {
            auto val = static_cast<bool>(entry);
            ar <=> ::articuno::next(val, flags);
        }
    }
}  // namespace articuno::serde
