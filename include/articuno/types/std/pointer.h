#pragma once

#include "../../concepts.h"
#include "../../dereferencer.h"
#include "../../format_error.h"
#include "../../illegal_indirection_error.h"
#include "../../index.h"
#include "../../instantiator.h"
#include "../../next.h"
#include "../../null.h"
#include "../../self.h"

namespace articuno::serde {
	template <::articuno::serializing_archive Archive, class T>
	void serde(Archive& ar, T*& value, ::articuno::value_flags flags) {
		if (value) {
			dereference<Archive, T>(ar, value, flags);
		} else {
			if (flags.has(::articuno::value_flags::required)) {
				throw ::articuno::format_error("Required field was not set; pointer is null.");
			} else if (flags.has(::articuno::value_flags::nullable)) {
				ar <=> ::articuno::null(flags);
			}
		}
	}

	template <::articuno::deserializing_archive Archive, class T>
	void serde(Archive& ar, T*& value, ::articuno::value_flags flags) {
		if (!Archive::flags.has(::articuno::archive_flags::allow_indirection) &&
			!flags.has(::articuno::value_flags::allow_indirection)) {
			throw ::articuno::illegal_indirection_error("Pointer used without indirection enabled.");
		}

		if (ar.is_sequence()) {
			auto* memory = instantiate<Archive, T>(ar, flags, ar.size());
			for (::std::size_t i = 0; i < ar.size(); ++i) {
				ar <=> ::articuno::next(memory[i], flags);
			}
			value = memory;
		} else {
			auto* memory = instantiate<Archive, T>(ar, flags);
			ar <=> ::articuno::self(*memory, flags);
			value = memory;
		}
	}

	template <::articuno::serializing_archive Archive>
	void serde(Archive& ar, const nullptr_t&, ::articuno::value_flags flags) {
		if (flags.has(::articuno::value_flags::required)) {
			throw ::articuno::format_error("Required field was not set; pointer is null.");
		} else if (flags.has(::articuno::value_flags::nullable)) {
			ar <=> ::articuno::null(flags);
		}
	}

	template <::articuno::deserializing_archive Archive>
		void serde(Archive&, nullptr_t&, ::articuno::value_flags flags) {
		if (!Archive::flags.has(::articuno::archive_flags::allow_indirection) &&
			!flags.has(::articuno::value_flags::allow_indirection)) {
			throw ::articuno::illegal_indirection_error("Pointer used without indirection enabled.");
		}
	}
}
