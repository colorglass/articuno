#pragma once

#include <utility>

#include "../../concepts.h"
#include "../../index.h"

namespace articuno::serde {
	template <::articuno::serializing_archive Archive, class First, class Second>
	void serde(Archive& ar, const ::std::pair<First, Second>& value, ::articuno::value_flags flags) {
        ar <=> ::articuno::sequence(flags);
		ar <=> ::articuno::index(0, value.first, flags);
		ar <=> ::articuno::index(1, value.second, flags);
	}

	template <::articuno::deserializing_archive Archive, class First, class Second>
	void serde(Archive& ar, ::std::pair<First, Second>& value, ::articuno::value_flags flags) {
        if (ar <=> ::articuno::sequence(flags)) {
            ar <=> ::articuno::index(0, value.first, flags);
            ar <=> ::articuno::index(1, value.second, flags);
        }
	}
}
