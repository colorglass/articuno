#pragma once

#include <stack>

#include "std.h"

namespace articuno::serde {
	template <::articuno::archive Archive, class T, class Container>
	void serde(Archive& ar, ::std::stack<T, Container>& value, ::articuno::value_flags flags) {
		ar <=> ::articuno::self(reinterpret_cast<Container&>(value), flags);
	}
}
