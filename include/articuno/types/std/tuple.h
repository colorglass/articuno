#pragma once

#include <tuple>

#include "../../concepts.h"
#include "../../next.h"

namespace articuno::serde {
	namespace detail {
		template <::articuno::archive Archive, ::std::size_t Index, class... Ts>
		void serde(Archive& ar, ::std::tuple<Ts...>& value, ::articuno::value_flags flags) {
			if constexpr (Index < sizeof...(Ts)) {
				ar <=> ::articuno::next(::std::get<Index>(value), flags);
				serde<Archive, Index + 1>(ar, value);
			}
		}
	}

	template <::articuno::archive Archive, class... Ts>
	void serde(Archive& ar, ::std::tuple<Ts...>& value, ::articuno::value_flags flags) {
        if (ar <=> ::articuno::sequence(flags)) {
            serde<Archive, 0, Ts...>(ar, value, flags);
        }
	}
}
