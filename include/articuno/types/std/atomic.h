#pragma once

#include <atomic>

#include "../../concepts.h"
#include "../../self.h"

namespace articuno::serde {
	template <::articuno::serializing_archive Archive, class T>
	void serde(Archive& ar, const ::std::atomic<T>& value, ::articuno::value_flags flags) {
		T val = value.load();
		ar <=> ::articuno::self(val, flags);
	}

	template <::articuno::deserializing_archive Archive, class T>
	void serde(Archive& ar, ::std::atomic<T>& value, ::articuno::value_flags flags) {
		T val;
		ar <=> ::articuno::self(val, flags);
		value.store(val);
	}
}
