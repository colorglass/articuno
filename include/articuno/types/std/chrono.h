#pragma once

#include <chrono>

#include "../../concepts.h"
#include "../../self.h"
#include "../../tags/chrono.h"

namespace articuno::serde {
	template <::articuno::serializing_archive Archive, class Rep, class Period>
	void serde(Archive& archive, const ::std::chrono::duration<Rep, Period>& value, ::articuno::value_flags flags) {
		auto count = value.count();
		archive <=> ::articuno::self(count, flags);
	}

	template <::articuno::deserializing_archive Archive, class Rep, class Period>
	void serde(Archive& archive, ::std::chrono::duration<Rep, Period>& value, ::articuno::value_flags flags) {
		Rep count;
		archive <=> ::articuno::self(count, flags);
		value = ::std::chrono::duration<Rep, Period>(count);
	}

	template <::articuno::serializing_archive Archive, class Rep, class Period>
	requires (::gluino::subtype_of<typename Archive::serde_tag_tag, ::articuno::tags::chrono_duration_epoch<::gluino::top>>)
	void serde(Archive& archive, const ::std::chrono::duration<Rep, Period>& value, ::articuno::value_flags flags) {
		auto dur = ::std::chrono::duration_cast<typename Archive::serde_tag_tag::duration_type>(value);
		auto count = dur.count();
		archive <=> ::articuno::self(count, flags);
	}

	template <::articuno::deserializing_archive Archive, class Rep, class Period>
	requires (::gluino::subtype_of<typename Archive::serde_tag_type, ::articuno::tags::chrono_duration_epoch<::gluino::top>>)
	void serde(Archive& archive, ::std::chrono::duration<Rep, Period>& value, ::articuno::value_flags flags) {
		typename Archive::serde_tag_type::duration_type::rep count;
		archive <=> ::articuno::self(count, flags);
		typename Archive::serde_tag_type::duration_type dur(count);
		value = ::std::chrono::duration_cast<::std::chrono::duration<Rep, Period>>(dur);
	}

	// TODO: Duration as string.

	template <::articuno::serializing_archive Archive, class Clock, class Duration>
	void serde(Archive& archive, const ::std::chrono::time_point<Clock, Duration>& value, ::articuno::value_flags flags) {
		auto seconds = ::std::chrono::duration_cast<::std::chrono::duration<double_t>>(value.time_since_epoch());
		archive <=> ::articuno::self(seconds, flags + ::articuno::value_flags::disable_tracking);
	}

	template <::articuno::deserializing_archive Archive, class Clock, class Duration>
	void serde(Archive& archive, ::std::chrono::time_point<Clock, Duration>& value, ::articuno::value_flags flags) {
		Duration duration;
		archive <=> ::articuno::self(duration, flags);
		value = ::std::chrono::time_point<Clock, Duration>(duration);
	}

	// TODO: Time point as string.

	template <::articuno::serializing_archive Archive, class Duration>
	void serde(Archive& archive, const ::std::chrono::hh_mm_ss<Duration>& value, ::articuno::value_flags flags) {
		auto seconds = ::std::chrono::duration_cast<::std::chrono::duration<double_t>>(value.to_duration());
		archive <=> ::articuno::self(seconds, flags + ::articuno::value_flags::disable_tracking);
	}

	template <::articuno::deserializing_archive Archive, class Duration>
	void serde(Archive& archive, ::std::chrono::hh_mm_ss<Duration>& value, ::articuno::value_flags flags) {
		double_t val;
		archive <=> ::articuno::self(val, flags);
		auto in = ::std::chrono::duration<double_t>(val);
		value = ::std::chrono::hh_mm_ss<Duration>(::std::chrono::duration_cast<Duration>(in));
	}

	// TODO: Time of day as string.
}
