#pragma once

#include <deque>

#include "../../concepts.h"
#include "../../next.h"
#include "../../sequence.h"

namespace articuno::serde {
	template <::articuno::archive Archive, class T, class Alloc>
	void serde(Archive& ar, ::std::deque<T, Alloc>& value, ::articuno::value_flags flags) {
        if (ar <=> ::articuno::sequence(flags)) {
            if constexpr (::articuno::deserializing_archive<Archive>) {
                value.clear();
                value.resize(ar.size());
            }
            for (auto& entry : value) {
                ar <=> ::articuno::next(entry, flags);
            }
        }
	}
}
