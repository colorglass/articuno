#pragma once

#include <array>

#include "../../concepts.h"
#include "../../format_error.h"
#include "../../index.h"
#include "../../narrowing_error.h"
#include "../../next.h"

namespace articuno::serde {
    template <::articuno::archive Archive, class T, ::std::size_t N>
    void serde(Archive& ar, ::std::array<T, N>& value,
               ::articuno::value_flags flags) {
        if (!(ar <=> ::articuno::sequence(flags))) {
            return false;
        }
        if constexpr (::articuno::deserializing_archive<Archive>) {
            if (N > ar.size() && flags.has(::articuno::value_flags::required)) {
                throw ::articuno::format_error(
                    "Missing required fields, sequence contents less than "
                    "array length.");
            }
            if (N < ar.size() &&
                (flags.has(::articuno::value_flags::disallow_narrowing) ||
                 (Archive::flags.none(
                      ::articuno::archive_flags::disallow_narrowing) &&
                  flags.none(::articuno::value_flags::allow_narrowing)))) {
                throw ::articuno::narrowing_error(
                    "Attempting to deserialize sequence with more content than "
                    "target array.");
            }
        }
        for (auto& entry : value) {
            ar <=> ::articuno::next(entry, flags);
        }
    }

    template <class T, ::std::size_t N>
    using primitive_array = T[N];

    template <::articuno::archive Archive, class T, ::std::size_t N>
    void serde(Archive& ar, ::articuno::serde::primitive_array<T, N>& value,
               ::articuno::value_flags flags) {
        ar <=> ::articuno::sequence(flags);
        if constexpr (::articuno::deserializing_archive<Archive>) {
            if (N > ar.size() && flags.has(::articuno::value_flags::required)) {
                throw ::articuno::format_error(
                    "Missing required fields, sequence contents less than "
                    "array length.");
            }
            if (N < ar.size() &&
                (flags.has(::articuno::value_flags::disallow_narrowing) ||
                 (Archive::flags.none(
                      ::articuno::archive_flags::disallow_narrowing) &&
                  flags.none(::articuno::value_flags::allow_narrowing)))) {
                throw ::articuno::narrowing_error(
                    "Attempting to deserialize sequence with more content than "
                    "target array.");
            }
        }
        for (::std::size_t i = 0; i < N; ++i) {
            ar <=> ::articuno::index(i, value[i], flags);
        }
    }
}  // namespace articuno::serde
