#pragma once

#include "../../concepts.h"
#include "../../self.h"

namespace articuno::serde {
	template <::articuno::archive Archive, class T>
	requires (::std::is_enum_v<T>)
	void serde(Archive& ar, T& value, ::articuno::value_flags flags) {
	    ar <=> ::articuno::self(reinterpret_cast<::std::underlying_type_t<T>&>(value), flags);
	}
}
