#pragma once

#include <list>

#include "../../concepts.h"
#include "../../next.h"

namespace articuno::serde {
	template <::articuno::serializing_archive Archive, class T, class Alloc>
	void serde(Archive& ar, const ::std::list<T, Alloc>& value, ::articuno::value_flags flags) {
        ar <=> ::articuno::sequence(flags);
		for (auto& entry : value) {
			ar <=> ::articuno::next(entry, flags);
		}
	}

	template <::articuno::deserializing_archive Archive, class T, class Alloc>
	void serde(Archive& ar, ::std::list<T, Alloc>& value, ::articuno::value_flags flags) {
        if (!(ar <=> ::articuno::sequence(flags))) {
            return;
        }
		value.clear();
		for (::std::size_t i = 0; i < ar.size(); ++i) {
			T val;
			ar <=> ::articuno::next(val, flags);
			if constexpr (::std::move_constructible<T>) {
				value.emplace_back(::std::move(val));
			} else if constexpr (::std::copy_constructible<T>) {
				value.emplace_back(val);
			} else {
				static_assert(::gluino::dependent_false<T>, "Entries of list must be move or copy constructible.");
			}
		}
	}
}
