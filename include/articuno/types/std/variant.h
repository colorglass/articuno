#pragma once

#include <variant>

#include "../../concepts.h"
#include "../../kv.h"

namespace articuno::serde {
	namespace detail {
		template <::articuno::deserializing_archive Archive, ::std::size_t Index, class... Ts>
		static void load([[maybe_unused]] Archive& archive, [[maybe_unused]] ::std::variant<Ts...>& value,
			[[maybe_unused]] ::articuno::value_flags flags, [[maybe_unused]] ::std::size_t index) {
			if constexpr (Index < sizeof...(Ts)) {
				if (Index == index) {
					auto& element = value.template emplace<Index>();
					archive <=> ::articuno::kv(element, "value",
									 flags + ::articuno::value_flags::required);
					return;
				}
				load<Archive, Index + 1, Ts...>(archive, value, flags, index);
			}
		}
	}

	template <::articuno::serializing_archive Archive, class... Ts>
	void serde(Archive& ar, const ::std::variant<Ts...>& value, ::articuno::value_flags flags) {
        auto index = value.index();
		ar <=> ::articuno::kv(index, "type", flags);
		::std::visit([&](auto&& arg) {
			ar <=> ::articuno::kv(arg, "value", flags);
		}, value);
	}

	template <::articuno::deserializing_archive Archive, class... Ts>
	void serde(Archive& ar, ::std::variant<Ts...>& value, ::articuno::value_flags flags) {
		::std::size_t index;
		ar <=> ::articuno::kv(index, "type", flags + ::articuno::value_flags::required);
		::articuno::serde::detail::load<Archive, 0, Ts...>(ar, value, flags, index);
	}
}
