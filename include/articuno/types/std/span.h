#pragma once

#include <span>

#include "../../concepts.h"

namespace articuno::serde {
	template <::articuno::serializing_archive Archive, class T, ::std::size_t N>
	void serde(Archive& ar, const ::std::span<T, N>& value, ::articuno::value_flags flags) {
        ar <=> ::articuno::sequence(flags);
		for (auto& entry : value) {
			ar <=> ::articuno::next(value, flags);
		}
	}

	template <::articuno::deserializing_archive Archive, class T, ::std::size_t N>
	void serde(Archive& ar, const ::std::span<T, N>& value, ::articuno::value_flags flags) {
        if (!(ar <=> ::articuno::sequence(flags))) {
            return;
        }
		if constexpr (::articuno::deserializing_archive<Archive>) {
			if (value.size() > ar.size() && flags.has(::articuno::value_flags::required)) {
				throw ::articuno::format_error("Missing required fields, sequence contents less than span length.");
			}
			if (value.size() < ar.size() && (flags.has(::articuno::value_flags::disallow_narrowing)
									 || (Archive::flags.none(::articuno::archive_flags::disallow_narrowing) &&
											flags.none(::articuno::value_flags::allow_narrowing)))) {
				throw ::articuno::narrowing_error("Attempting to deserialize sequence with more content than span array.");
			}
		}
		for (auto& entry : value) {
			ar <=> ::articuno::next(value, flags);
		}
	}
}
