#pragma once

#include <unordered_set>

#include "../../concepts.h"
#include "../../index.h"
#include "../../next.h"

namespace articuno::serde {
	template <::articuno::serializing_archive Archive, class T, class Hash, class Eq, class Alloc>
	void serde(Archive& ar, const ::std::unordered_set<T, Hash, Eq, Alloc>& value, ::articuno::value_flags flags) {
        ar <=> ::articuno::sequence(flags);
		for (auto& entry : value) {
			ar <=> ::articuno::next(entry, flags);
		}
	}

	template <::articuno::deserializing_archive Archive, class T, class Hash, class Eq, class Alloc>
	void serde(Archive& ar, ::std::unordered_set<T, Hash, Eq, Alloc>& value, ::articuno::value_flags flags) {
        if (!(ar <=> ::articuno::sequence(flags))) {
            return false;
        }
		value.clear();
		value.reserve(ar.size());
		for (::std::size_t i = 0; i < ar.size(); ++i) {
			T val;
			ar <=> ::articuno::index(i, val, flags);
			if constexpr (::std::move_constructible<T>) {
				value.emplace(::std::move(val));
			} else if constexpr (::std::copy_constructible<T>) {
				value.emplace(val);
			} else {
				static_assert(::gluino::dependent_false<T>, "Entries of unordered_set must be move or copy constructible.");
			}
		}
	}
}
