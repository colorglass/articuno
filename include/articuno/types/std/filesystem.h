#pragma once

#include <filesystem>

#include "string.h"

namespace articuno::serde {
	template <::articuno::serializing_archive Archive>
	void serde(Archive& ar, const ::std::filesystem::path& value, ::articuno::value_flags flags) {
		auto str = value.template generic_string<typename ::std::filesystem::path::value_type>();
		ar <=> ::articuno::self(str, flags);
	}

	template <::articuno::deserializing_archive Archive>
	void serde(Archive& ar, ::std::filesystem::path& value, ::articuno::value_flags flags) {
		::std::basic_string<typename ::std::filesystem::path::value_type> str;
		ar <=> ::articuno::self(str, flags);
		value = ::std::move(str);
	}
}
