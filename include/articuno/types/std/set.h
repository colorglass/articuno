#pragma once

#include <set>

#include "../../concepts.h"
#include "../../index.h"

namespace articuno::serde {
	template <::articuno::archive Archive, class T, class Less, class Alloc>
	void serde(Archive& ar, ::std::set<T, Less, Alloc>& value, ::articuno::value_flags flags) {
        if (ar <=> ::articuno::sequence(flags)) {
            for (auto& entry : value) {
                ar <=> ::articuno::next(entry, flags);
            }
        }
	}
}
