#pragma once

#include <bitset>

#include "../../concepts.h"
#include "../../next.h"
#include "../../sequence.h"

namespace articuno::serde {
    template <::articuno::serializing_archive Archive, ::std::size_t N>
    void serde(Archive& ar, const ::std::bitset<N>& value, ::articuno::value_flags flags) {
        for (::std::size_t i = 0; i < N; ++i) {
            bool val = value[i];
            ar <=> ::articuno::next(val, flags);
        }
    }

    template <::articuno::deserializing_archive Archive, ::std::size_t N>
    void serde(Archive& ar, ::std::bitset<N>& value, ::articuno::value_flags flags) {
        if (!(ar <=> ::articuno::sequence(flags))) {
            return false;
        }
        if (N > ar.size() && flags.has(::articuno::value_flags::required)) {
            throw ::articuno::format_error("Missing required fields, sequence contents less than bitset length.");
        }
        if (N < ar.size() && (flags.has(::articuno::value_flags::disallow_narrowing) ||
                              (Archive::flags.none(::articuno::archive_flags::disallow_narrowing) &&
                               flags.none(::articuno::value_flags::allow_narrowing)))) {
            throw ::articuno::narrowing_error(
                "Attempting to deserialize sequence with more content than bitset array.");
        }
        for (::std::size_t i = 0; i < N; ++i) {
            bool val;
            ar <=> ::articuno::index(i, val, flags);
            value.set(i, val);
        }
    }
}  // namespace articuno::serde
