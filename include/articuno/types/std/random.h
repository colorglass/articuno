#pragma once

#include <random>
#include <sstream>

#include "iostream.h"

namespace articuno::serde {
	template <::articuno::serializing_archive Archive, class UIntType, ::std::size_t w, ::std::size_t n, ::std::size_t m, ::std::size_t r,
		UIntType a, ::std::size_t u, UIntType d, ::std::size_t s, UIntType b, ::std::size_t t, UIntType c, ::std::size_t l, UIntType f>
	void serde(Archive& ar, const ::std::mersenne_twister_engine<UIntType, w, n, m, r, a, u, d, s, b, t, c, l, f>& value,
			::articuno::value_flags flags) {
		::std::stringstream out;
		out << value;
		out.seekg(0);
		ar <=> ::articuno::self(out, flags);
	}

	template <::articuno::deserializing_archive Archive, class UIntType, ::std::size_t w, ::std::size_t n, ::std::size_t m, ::std::size_t r,
		UIntType a, ::std::size_t u, UIntType d, ::std::size_t s, UIntType b, ::std::size_t t, UIntType c, ::std::size_t l, UIntType f>
	void serde(Archive& ar,
		::std::mersenne_twister_engine<UIntType, w, n, m, r, a, u, d, s, b, t, c, l, f>& value, ::articuno::value_flags flags) {
		::std::stringstream in;
		ar <=> ::articuno::self(in, flags);
		in >> value;
	}

	template <::articuno::serializing_archive Archive, class UIntType, UIntType a, UIntType c, UIntType m>
	static void serde(Archive& archive, const ::std::linear_congruential_engine<UIntType, a, c, m>& value,
			::articuno::value_flags flags) {
		::std::stringstream out;
		out << value;
		archive <=> ::articuno::self(out, flags);
	}

	template <::articuno::deserializing_archive Archive, class UIntType, UIntType a, UIntType c, UIntType m>
	static void serde(Archive& archive, ::std::linear_congruential_engine<UIntType, a, c, m>& value,
			::articuno::value_flags flags) {
		::std::stringstream in;
		archive <=> ::articuno::self(in, flags);
		in >> value;
	}

	template <::articuno::serializing_archive Archive, class UIntType, std::size_t w, std::size_t s, std::size_t r>
	static void serde(Archive& archive, const ::std::subtract_with_carry_engine<UIntType, w, s, r>& value,
			::articuno::value_flags flags) {
		::std::stringstream out;
		out << value;
		archive <=> ::articuno::self(out.str(), archive.value_flags());
	}

	template <::articuno::deserializing_archive Archive, class UIntType, std::size_t w, std::size_t s, std::size_t r>
	static void serde(Archive& archive, ::std::subtract_with_carry_engine<UIntType, w, s, r>& value,
			::articuno::value_flags flags) {
		::std::stringstream in;
		archive <=> ::articuno::self(in, flags);
		in >> value;
	}
}
