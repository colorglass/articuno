#pragma once

#include <complex>

#include "../../kv.h"
#include "../../tags/tags.h"
#include "utility.h"

namespace articuno::serde {
	template <::articuno::serializing_archive Archive, class T>
	void serde(Archive& ar, ::std::complex<T>& value, value_flags flags) {
		::std::pair<T, T> val{value.real(), value.imag()};
		ar <=> ::articuno::self(val, flags);
	}

	template <::articuno::deserializing_archive Archive, class T>
	void serde(Archive& ar, ::std::complex<T>& value, value_flags flags) {
		::std::pair<T, T> val{T{}, T{}};
		ar <=> ::articuno::self(val, flags);
		value = {val, val};
	}

	template <::articuno::serializing_archive Archive, class T>
	requires (::articuno::tags::complex_map_options<typename Archive::serde_tag_type::options_type>)
	void serde(Archive& ar, ::std::complex<T>& value, value_flags flags) {
		auto real = value.real();
		auto imag = value.imag();
		ar <=> ::articuno::kv(real, Archive::serde_tag_type::options_type::real_key,
					Archive::serde_tag_type::options_type::real_namespace, flags);
		ar <=> ::articuno::kv(imag, Archive::serde_tag_type::options_type::imag_key,
					Archive::serde_tag_type::options_type::imag_namespace, flags);
	}

	template <::articuno::deserializing_archive Archive, class T>
	requires (::articuno::tags::complex_map_options<typename Archive::serde_tag_type::options_type>)
	void serde(Archive& ar, ::std::complex<T>& value, value_flags flags) {
		T real;
		T imag;
		ar <=> ::articuno::kv(real, Archive::serde_tag_type::options_type::real_key,
					Archive::serde_tag_type::options_type::real_namespace, flags);
		ar <=> ::articuno::kv(imag, Archive::serde_tag_type::options_type::imag_key,
					Archive::serde_tag_type::options_type::imag_namespace, flags);
		value = ::std::complex(real, imag);
	}
}
