#pragma once

#include <queue>

#include "std.h"

namespace articuno::serde {
	template <::articuno::archive Archive, class T, class Container>
	void serde(Archive& ar, ::std::queue<T, Container>& value, ::articuno::value_flags flags) {
		ar <=> ::articuno::self(reinterpret_cast<Container&>(value), flags);
	}

	template <::articuno::serializing_archive Archive, class T, class Container, class Less>
	void serde(Archive& ar, ::std::priority_queue<T, Container, Less>& value, ::articuno::value_flags flags) {
		ar <=> ::articuno::self(reinterpret_cast<Container&>(value), flags);
	}

	template <::articuno::deserializing_archive Archive, class T, class Container, class Less>
	void serde(Archive& ar, ::std::priority_queue<T, Container, Less>& value, ::articuno::value_flags flags) {
        if (!(ar <=> ::articuno::sequence(flags))) {
            return;
        }
		for (::std::size_t i = 0; i < ar.size(); ++i) {
			T val;
			ar <=> ::articuno::index(i, val, flags);
			if constexpr (::std::move_constructible<T>) {
				value.emplace(::std::move(val));
			} else if constexpr (::std::copy_constructible<T>) {
				value.emplace(val);
			} else {
				static_assert(::gluino::dependent_false<T>, "Entries of priority_queue must be move or copy constructible.");
			}
		}
	}
}
