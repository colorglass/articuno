#pragma once

#include <tsl/ordered_set.h>

#include <gluino/string_cast.h>
#include <gluino/utility.h>

#include "../../concepts.h"
#include "../../kv.h"
#include "../../next.h"
#include "../../object.h"
#include "../../sequence.h"

namespace articuno::serde {
    template <::articuno::serializing_archive Archive, class T, class Hash, class Eq, class Alloc, class ValueTypeContainer, class IndexType>
    void serde(Archive& ar, const ::tsl::ordered_set<T, Hash, Eq, Alloc, ValueTypeContainer, IndexType>& value, ::articuno::value_flags flags) {
        ar <=> ::articuno::sequence(flags);
        for (auto& entry : value) {
            ar <=> ::articuno::next(entry, flags);
        }
    }

    template <::articuno::deserializing_archive Archive, class T, class Hash, class Eq, class Alloc, class ValueTypeContainer, class IndexType>
    void serde(Archive& ar, ::tsl::ordered_set<T, Hash, Eq, Alloc, ValueTypeContainer, IndexType>& value, ::articuno::value_flags flags) {
        value.clear();
        if (!(ar <=> ::articuno::sequence(flags))) {
            return;
        }
        value.reserve(ar.size());
        for (::std::size_t i = 0; i < ar.size(); ++i) {
            T entry;
            ar <=> ::articuno::index(i, entry, flags);
            if constexpr (::std::move_constructible<decltype(entry)>) {
                value.emplace(::std::move(entry));
            } else if constexpr (::std::copy_constructible<decltype(entry)>) {
                value.emplace(entry);
            } else {
                static_assert(::gluino::dependent_false<T>, "Entries of unordered_map must be move or copy constructible.");
            }
        }
    }
}
