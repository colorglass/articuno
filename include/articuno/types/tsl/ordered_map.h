#pragma once

#include <tsl/ordered_map.h>

#include <gluino/string_cast.h>
#include <gluino/utility.h>

#include "../../concepts.h"
#include "../../kv.h"
#include "../../next.h"
#include "../../object.h"
#include "../../sequence.h"
#include "../std/utility.h"

namespace articuno::serde {
    template <::articuno::serializing_archive Archive, class K, class V, class Hash, class Eq, class Alloc, class ValueTypeContainer, class IndexType>
    void serde(Archive& ar, const ::tsl::ordered_map<K, V, Hash, Eq, Alloc, ValueTypeContainer, IndexType>& value, ::articuno::value_flags flags) {
        ar <=> ::articuno::sequence(flags);
        for (auto& entry : value) {
            ar <=> ::articuno::next(entry, flags);
        }
    }

    template <::articuno::deserializing_archive Archive, class K, class V, class Hash, class Eq, class Alloc, class ValueTypeContainer, class IndexType>
    void serde(Archive& ar, ::tsl::ordered_map<K, V, Hash, Eq, Alloc, ValueTypeContainer, IndexType>& value, ::articuno::value_flags flags) {
        value.clear();
        if (!(ar <=> ::articuno::sequence(flags))) {
            return;
        }
        value.reserve(ar.size());
        for (::std::size_t i = 0; i < ar.size(); ++i) {
            std::pair<K, V> entry;
            ar <=> ::articuno::index(i, entry, flags);
            if constexpr (::std::move_constructible<decltype(entry)>) {
                value.emplace(::std::move(entry));
            } else if constexpr (::std::copy_constructible<decltype(entry)>) {
                value.emplace(entry);
            } else {
                static_assert(::gluino::dependent_false<K>, "Entries of unordered_map must be move or copy constructible.");
            }
        }
    }
}
