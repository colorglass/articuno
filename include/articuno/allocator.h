#pragma once

#include <memory>

#include <gluino/type_traits.h>

#include "value_flags.h"

namespace articuno::serde {
	template <::articuno::archive Archive, class T>
	[[nodiscard]] inline ::gluino::uncvref_t<T>* allocate(Archive&, ::articuno::value_flags, ::std::size_t count = 1) {
		::std::allocator<::gluino::uncvref_t<T>> allocator;
		return allocator.allocate(count);
	}

	template <::articuno::archive Archive, class T>
	[[nodiscard]] inline ::gluino::unconst_t<T>* construct_at(Archive&, T* memory, ::articuno::value_flags) {
		return ::articuno::access::construct_at<::gluino::unconst_t<T>>(memory);
	}
}
