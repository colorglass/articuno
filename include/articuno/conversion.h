#pragma once

#include "archive_flags.h"
#include "narrowing_error.h"
#include "rounding_error.h"
#include "value_flags.h"

namespace articuno {
	class conversion {
	public:
		conversion() = delete;

		conversion(const conversion&) = delete;

		conversion(conversion&&) = delete;

		template <class In, class Out, ::articuno::archive_flags Flags = ::articuno::archive_flags{}>
		static inline void convert(In& in, Out& out, ::articuno::value_flags) {
			out = static_cast<Out>(in);
		}

		template <::std::integral In, ::std::integral Out, ::articuno::archive_flags Flags = ::articuno::archive_flags{}>
		static void convert(In in, Out& out, ::articuno::value_flags value_flags) {
			auto out_trial = static_cast<Out>(in);
			if (in != out_trial) {
				if (!is_narrowing_allowed<Flags>(value_flags)) {
					throw ::articuno::narrowing_error("Conversion from document value to C++ type would result in "
						"narrowing, which is not allowed according to flags.");
				} else if (in > out_trial) {
					out_trial = ::std::numeric_limits<Out>::max();
				} else {
					out_trial = ::std::numeric_limits<Out>::min();
				}
			}
			out = out_trial;
		}

		template <::std::signed_integral In, ::std::unsigned_integral Out, ::articuno::archive_flags Flags = ::articuno::archive_flags{}>
		static void convert(In in, Out& out, ::articuno::value_flags value_flags) {
			if (in < 0) {
				if (!is_narrowing_allowed<Flags>(value_flags)) {
					throw ::articuno::narrowing_error("Conversion from document value to C++ type would result in "
						"narrowing, which is not allowed according to archive flags.");
				}
				out = 0;
			} else {
				out = static_cast<Out>(in);
			}
		}

		template <::std::unsigned_integral In, ::std::signed_integral Out>
		static void convert(In in, Out& out, ::articuno::value_flags value_flags) {
			if (in > ::std::numeric_limits<Out>::max()) {
				if (!is_narrowing_allowed(value_flags)) {
					throw ::articuno::narrowing_error("Conversion from document value to C++ type would result in "
						"narrowing, which is not allowed according to archive flags.");
				}
				out = ::std::numeric_limits<Out>::max();
			} else {
				out = static_cast<Out>(in);
			}
		}

		template <::std::floating_point In, ::std::floating_point Out, ::articuno::archive_flags Flags = ::articuno::archive_flags{}>
		static void convert(In in, Out& out, ::articuno::value_flags value_flags) {
			if (!is_narrowing_allowed<Flags>(value_flags) &&
				(::std::numeric_limits<Out>::max() < in || ::std::numeric_limits<Out>::min() > in)) {
				throw ::articuno::narrowing_error("Conversion from document value to C++ type would result in "
					"narrowing, which is not allowed according to archive flags.");
			}
			out = static_cast<Out>(in);
		}

		template <::std::floating_point In, ::std::integral Out, ::articuno::archive_flags Flags = ::articuno::archive_flags{}>
		static void convert(In in, Out& out, ::articuno::value_flags value_flags) {
			if (!is_narrowing_allowed<Flags>(value_flags) &&
				(::std::numeric_limits<Out>::max() < in || ::std::numeric_limits<Out>::min() > in)) {
				throw ::articuno::narrowing_error("Conversion from document value to C++ type would result in "
					"narrowing, which is not allowed according to archive flags.");
			}
			auto out_trial = static_cast<Out>(in);
			if (in != out_trial && Flags.all(::articuno::archive_flags::enum_type::disallow_rounding)) {
				throw ::articuno::rounding_error("Conversion from document value to C++ would result in "
					"rounding, which is not allowed according to archive flags.");
			}
			out = out_trial;
		}

		template <::std::integral In, ::std::floating_point Out, ::articuno::archive_flags Flags = ::articuno::archive_flags{}>
		static void convert(In in, Out& out, ::articuno::value_flags value_flags) {
			auto out_trial = static_cast<Out>(in);
			if (in != out_trial && Flags.all(::articuno::archive_flags::enum_type::disallow_rounding)) {
				throw ::articuno::rounding_error("Conversion from document value to C++ would result in "
					"rounding, which is not allowed according to archive flags.");
			}
			out = out_trial;
		}

	private:
		template <::articuno::archive_flags Flags = ::articuno::archive_flags{}>
		[[nodiscard]] inline static bool is_narrowing_allowed(::articuno::value_flags value_flags) noexcept {
			return !(value_flags.all(::articuno::value_flags::disallow_narrowing) ||
					 (Flags.all(::articuno::archive_flags::disallow_narrowing) &&
						 value_flags.none(::articuno::value_flags::enum_type::allow_narrowing)));
		}
	};
}
