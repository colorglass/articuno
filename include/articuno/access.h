#pragma once

#include "concepts.h"

namespace articuno {
	class access {
	public:
		template <class T, class... Args>
		static T* construct_at(T* memory, Args... args) noexcept(noexcept(T(args...))) {
			return new(memory) T(args...);
		}

		template <class T>
		static void destroy(T* object) noexcept(noexcept(delete object)) {
			delete object;
		}

		template <::articuno::deserializing_archive Archive, class T>
		requires (::articuno::intrusive_deserializable<T, Archive> &&
		    !::articuno::nonintrusive_deserializable<T, Archive>)
		static void serde(Archive& ar, T& value, value_flags flags) {
			value.serde(ar, flags);
		}

		template <::articuno::deserializing_archive Archive, class T>
		requires (::articuno::nonintrusive_deserializable<T, Archive>)
		static void serde(Archive& ar, T& value, value_flags flags) {
			::articuno::serde::serde_adl(ar, value, flags);
		}

		template <::articuno::serializing_archive Archive, class T>
		requires (::articuno::intrusive_serializable<T, Archive> &&
				 !::articuno::nonintrusive_serializable<T, Archive>)
		static void serde(Archive& ar, const T& value, value_flags flags) {
			const_cast<::gluino::unconst_t<T&>>(value).serde(ar, flags);
		}

		template <::articuno::serializing_archive Archive, class T>
		requires (::articuno::intrusive_serializable<T, Archive> &&
		    !::articuno::nonintrusive_serializable<T, Archive>)
		static void serde(Archive& ar, T& value, value_flags flags) {
			value.serde(ar, flags);
		}

		template <::articuno::serializing_archive Archive, class T>
		requires (::articuno::nonintrusive_serializable<T, Archive>)
		static void serde(Archive& ar, const T& value, value_flags flags) {
			::articuno::serde::serde_adl(ar, const_cast<::gluino::unconst_t<T&>>(value), flags);
		}

		template <::articuno::serializing_archive Archive, class T>
		requires (::articuno::nonintrusive_serializable<T, Archive>)
		static void serde(Archive& ar, T& value, value_flags flags) {
			::articuno::serde::serde_adl(ar, value, flags);
		}
	};
}
