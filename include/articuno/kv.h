#pragma once

#include <gluino/concepts.h>

#include "concepts.h"
#include "value_flags.h"

namespace articuno {
	template <class Value, ::gluino::any_string_type Key, ::gluino::string_like Namespace = std::string_view>
	class kv {
	public:
		using key_type = Key;
		using namespace_type = Namespace;
		using element_type = Value;
		using value_type = Value;
		using namespace_type = Namespace;

		constexpr kv(Value& value, Key key, ::articuno::value_flags flags = {}) : _key(key), _value(&value), _flags(flags) {
		}

		constexpr kv(Value& value, Namespace ns, Key key, ::articuno::value_flags flags = {}) : _namespace(ns), _key(key), _value(&value), _flags(flags) {
		}

		[[nodiscard]] constexpr const namespace_type& key_namespace() const noexcept {
			return _namespace;
		}

		[[nodiscard]] constexpr const key_type& key() const noexcept {
			return _key;
		}

		[[nodiscard]] constexpr value_type& value() noexcept {
			return *_value;
		}

		[[nodiscard]] constexpr ::articuno::value_flags flags() const noexcept {
			return _flags;
		}

	private:
		namespace_type _namespace{};
		key_type _key{};
		value_type* _value;
		::articuno::value_flags _flags;
	};
}
