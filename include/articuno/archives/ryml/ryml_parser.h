#pragma once

#include <format>

#include <gluino/string_cast.h>
#include <gluino/utility.h>
#include <ryml/ryml.hpp>

namespace articuno::ryml {
	template <class Char = char, class Traits = ::std::char_traits<Char>, class Alloc = ::std::allocator<Char>>
	class basic_ryml_parser {
	public:
		using value_type = Char;
		using traits_type = Traits;

		::ryml::Tree parse(::std::basic_istream<Char, Traits>& input) {
			::std::basic_string<Char, Traits, Alloc> raw(::std::istreambuf_iterator<Char, Traits>(input), {});
			_content = ::gluino::try_move(::gluino::string_cast<char>(raw));
			return ::ryml::parse(::ryml::to_csubstr(_content.c_str()));
		}

	private:
		::std::string _content;
	};

	using ryml_parser = ::articuno::ryml::basic_ryml_parser<char>;
	using wryml_parser = ::articuno::ryml::basic_ryml_parser<wchar_t>;
	using u8ryml_parser = ::articuno::ryml::basic_ryml_parser<char8_t>;
	using u16ryml_parser = ::articuno::ryml::basic_ryml_parser<char16_t>;
	using u32ryml_parser = ::articuno::ryml::basic_ryml_parser<char32_t>;

	template <class Char = char, class Traits = ::std::char_traits<Char>, class Alloc = ::std::allocator<Char>>
	using basic_yaml_parser = basic_ryml_parser<Char, Traits, Alloc>;

	using yaml_parser = ::articuno::ryml::basic_yaml_parser<char>;
	using wyaml_parser = ::articuno::ryml::basic_yaml_parser<wchar_t>;
	using u8yaml_parser = ::articuno::ryml::basic_yaml_parser<char8_t>;
	using u16yaml_parser = ::articuno::ryml::basic_yaml_parser<char16_t>;
	using u32yaml_parser = ::articuno::ryml::basic_yaml_parser<char32_t>;

	template <class Char = char, class Traits = ::std::char_traits<Char>, class Alloc = ::std::allocator<Char>>
	using basic_json_parser = ::articuno::ryml::basic_ryml_parser<Char, Traits, Alloc>;

	using json_parser = ::articuno::ryml::basic_json_parser<char>;
	using wjson_parser = ::articuno::ryml::basic_json_parser<wchar_t>;
	using u8json_parser = ::articuno::ryml::basic_json_parser<char8_t>;
	using u16json_parser = ::articuno::ryml::basic_json_parser<char16_t>;
	using u32json_parser = ::articuno::ryml::basic_json_parser<char32_t>;
}
