#pragma once

#include <parallel_hashmap/phmap.h>

#include <format>

#include "../sdk.h"
#include "ryml_emitter.h"

namespace articuno::ryml {
    template <class Emitter = ::articuno::ryml::yaml_emitter,
              ::articuno::archive_flags Flags = ::articuno::archive_flags{}, auto SerdeTag = ::articuno::default_tag{}>
    class yaml_sink {
       public:
        class yaml_oarchive {
           public:
            using size_type = ::std::size_t;
            using serde_tag_type = decltype(SerdeTag);
            static constexpr serde_tag_type serde_tag = SerdeTag;
            static constexpr ::articuno::archive_flags flags = Flags;
            static constexpr bool serializing = true;
            using emitter_type = Emitter;
            using value_type = typename emitter_type::value_type;
            using traits_type = typename emitter_type::traits_type;
            using stream_type = ::std::basic_istream<value_type, traits_type>;
            using archive_type = yaml_oarchive;
            using native_value_type = char;
            using native_string_type = ::std::string;

            inline yaml_oarchive(::ryml::NodeRef node,
                                 ::articuno::ryml::yaml_sink<emitter_type, Flags, SerdeTag>& root) noexcept
                : _node(node), _root(root) {}

            [[nodiscard]] inline size_type size() const noexcept { return _node.num_children(); }

            template <::gluino::arithmetic T>
            bool operator<=>(::articuno::self<T>&& value) {
                if constexpr (::std::is_same_v<bool, ::std::remove_cv_t<T>>) {
                    _node << ::ryml::fmt::boolalpha(value.value());
                } else {
                    _node << value.value();
                }
                return true;
            }

            template <class Traits = ::std::char_traits<char>, class Alloc = ::std::allocator<char>>
            bool operator<=>(::articuno::self<::std::basic_string<char, Traits, Alloc>>&& value) {
                _node << value.value();
                return true;
            }

            template <class Traits = ::std::char_traits<char>, class Alloc = ::std::allocator<char>>
            bool operator<=>(::articuno::self<::std::basic_string<char8_t, Traits, Alloc>>&& value) {
                _node << reinterpret_cast<const ::std::string&>(value.value());
                return true;
            }

            template <class Char, class Traits = ::std::char_traits<Char>, class Alloc = ::std::allocator<Char>>
            inline bool operator<=>(::articuno::self<::std::basic_string<Char, Traits, Alloc>>&& value) {
                auto encoded = ::gluino::claim(::gluino::string_cast<char>(value.value()));
                return *this <=> ::articuno::self(encoded, value.flags());
            }

            template <class T>
            requires(::articuno::custom_serializable<T, yaml_oarchive>) inline bool operator<=>(
                ::articuno::self<T>&& value) {
                ::articuno::serialize(*this, value.value(), value.flags());
                return true;
            }

            template <class T, class Key, class Namespace>
            requires(::articuno::serializable<T, yaml_oarchive>) bool operator<=>(
                ::articuno::kv<T, Key, Namespace>&& property) {
                _node |= ::ryml::MAP;
                decltype(auto) converted_key = ::gluino::string_cast<char>(property.key());
                auto child = _node.append_child();
                auto str = converted_key.data();
                child << ::ryml::key<const char*>(str);
                yaml_oarchive ar(child, _root);
                ar <=> ::articuno::self(property.value(), property.flags());
                return true;
            }

            template <class T, class Key, class Namespace>
            requires(::articuno::serializable<T, yaml_oarchive>) bool operator<=>(
                ::articuno::index<T, Key, Namespace>&& property) {
                _node |= ::ryml::SEQ;
                auto child = _node[property.position()];
                yaml_oarchive ar(child, _root);
                ar <=> ::articuno::self(property.value(), property.flags());
                return true;
            }

            template <class T>
            requires(::articuno::serializable<T, yaml_oarchive>) bool operator<=>(::articuno::next<T>&& value) {
                _node |= ::ryml::SEQ;
                auto child = _node.append_child();
                yaml_oarchive ar(child, _root);
                ar <=> ::articuno::self(value.value(), value.flags());
                return true;
            }

            bool operator<=>(::articuno::sequence&&) {
                _node |= ::ryml::SEQ;
                return true;
            }

            bool operator<=>(::articuno::object&&) {
                _node |= ::ryml::MAP;
                return true;
            }

            bool operator<=>(::articuno::null&&) {
                _node.clear_val();
                return true;
            }

           private:
            ::ryml::NodeRef _node;
            ::articuno::ryml::yaml_sink<Emitter, Flags, SerdeTag>& _root;
            ::articuno::value_flags _value_flags;
        };

        using emitter_type = Emitter;
        using value_type = typename emitter_type::value_type;
        using traits_type = typename emitter_type::traits_type;
        using stream_type = ::std::basic_ostream<value_type, traits_type>;
        using archive_type = yaml_oarchive;

        template <::std::enable_if_t<::std::is_default_constructible_v<emitter_type>, int> = 0>
        inline explicit yaml_sink(stream_type& output) noexcept(noexcept(emitter_type())) : _output(&output) {}

        inline yaml_sink(stream_type& output, emitter_type emitter) noexcept(noexcept(_emitter = emitter))
            : _output(&output), _emitter(emitter) {}

        inline void reset(stream_type& output) { _output = &output; }

        template <class T>
        requires(::articuno::serializable<T, archive_type>) yaml_sink& operator<<(T& value) { return write(value); }

        template <class T>
        requires(::articuno::serializable<T, archive_type>) yaml_sink& write(
            T& value, ::articuno::value_flags flags = ::articuno::value_flags::allow_indirection) {
            // TODO: When vcpkg catches up to modern ryml versions, switch to per-parser callback overrides.
            ::ryml::set_callbacks(
                ::ryml::Callbacks(nullptr, nullptr, nullptr,
                                  [](const char* msg, ::std::size_t msg_len, ::ryml::Location location, void*) {
                                      throw ::articuno::format_error(
                                          ::std::format("Error serializing YAML data at line {}, column {}: {}",
                                                        location.line, location.col, ::std::string_view(msg, msg_len)));
                                  }));
            auto root = ::ryml::Tree();
            archive_type ar(root.rootref(), *this);
            ar <=> ::articuno::self(value, flags);
            _emitter.emit(*_output, root);
            ::ryml::reset_callbacks();
            return *this;
        }

       private:
        stream_type* _output;
        emitter_type _emitter;
        ::phmap::flat_hash_map<::std::string_view, ::ryml::NodeRef> _anchors;
        ::phmap::flat_hash_map<::ryml::NodeRef, void*> _tracked;
    };
}  // namespace articuno::ryml
