#pragma once

#include <c4/format.hpp>
#include <gluino/string_cast.h>
#include <ryml/ryml_std.hpp>
#include <ryml/ryml.hpp>

namespace articuno::ryml {
	template <bool Json = false, class Char = char, class Traits = ::std::char_traits<Char>>
	class basic_ryml_emitter {
	public:
		using value_type = Char;
		using traits_type = Traits;

		static constexpr bool json_mode = Json;

		void emit(::std::basic_ostream<Char, Traits>& output, const ::ryml::Tree& root) {
			if constexpr (sizeof(value_type) == sizeof(char)) {
				reinterpret_cast<::std::basic_ostream<char, ::std::char_traits<char>>&>(output) << root;
			} else {
				::std::string out;
				::c4::substr sub;
				if constexpr (Json) {
					sub = ::ryml::emit_json(root, root.root_id(), sub, false);
					sub = ::c4::substr(out.data(), out.size());
					sub = ::c4::to_substr(out.data());
					::ryml::emit_json(root, root.root_id(), sub, true);
				} else {
					sub = ::ryml::emit(root, root.root_id(), sub, false);
					out.resize(sub.len);
					sub = ::c4::substr(out.data(), out.size());
					::ryml::emit(root, root.root_id(), sub, true);
				}
				output << ::gluino::string_cast<Char>(out);
			}
		}
	};

	template <class Char = char, class Traits = ::std::char_traits<Char>>
	using basic_yaml_emitter = basic_ryml_emitter<false, Char, Traits>;

	using yaml_emitter = basic_yaml_emitter<char>;
	using wyaml_emitter = basic_yaml_emitter<wchar_t>;
	using u8yaml_emitter = basic_yaml_emitter<char8_t>;
	using u16yaml_emitter = basic_yaml_emitter<char16_t>;
	using u32yaml_emitter = basic_yaml_emitter<char32_t>;

	template <class Char = char, class Traits = ::std::char_traits<Char>>
	using basic_json_emitter = basic_ryml_emitter<true, Char, Traits>;

	using json_emitter = basic_json_emitter<char>;
	using wjson_emitter = basic_json_emitter<wchar_t>;
	using u8json_emitter = basic_json_emitter<char8_t>;
	using u16json_emitter = basic_json_emitter<char16_t>;
	using u32json_emitter = basic_json_emitter<char32_t>;
}
