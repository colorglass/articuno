#pragma once

#include <parallel_hashmap/phmap.h>

#include <format>

#include "../sdk.h"
#include "ryml_parser.h"

namespace articuno::ryml {
    template <class Parser = ::articuno::ryml::yaml_parser,
              ::articuno::archive_flags Flags = ::articuno::archive_flags{}, auto SerdeTag = ::articuno::default_tag{}>
    class yaml_source {
       public:
        class yaml_iarchive {
           public:
            using size_type = ::std::size_t;
            using serde_tag_type = decltype(SerdeTag);
            static constexpr serde_tag_type serde_tag = SerdeTag;
            static constexpr ::articuno::archive_flags flags = Flags;
            static constexpr bool deserializing = true;
            using parser_type = Parser;
            using value_type = typename parser_type::value_type;
            using traits_type = typename parser_type::traits_type;
            using stream_type = ::std::basic_istream<value_type, traits_type>;
            using archive_type = yaml_iarchive;
            using native_value_type = char;
            using native_string_type = ::std::string;

            class iterator {
               public:
                using iterator_category = std::bidirectional_iterator_tag;
                using difference_type = std::ptrdiff_t;
                using value_type = ::std::pair<std::string, std::string>;
                using pointer = value_type*;
                using reference = value_type&;

                inline iterator& operator++() noexcept {
                    _node = _node.next_sibling();
                    if (_node.valid()) {
                        ::std::string_view key(_node.key().str, _node.key().len);
                        _key = {"", ::gluino::try_move(::gluino::string_cast<typename yaml_iarchive::value_type>(key))};
                    }
                    return *this;
                }

                inline iterator operator++(int) noexcept { return iterator(_node.next_sibling()); }

                inline iterator& operator--() noexcept {
                    _node = _node.prev_sibling();
                    if (_node.valid()) {
                        ::std::string_view key(_node.key().str, _node.key().len);
                        _key = {"", ::gluino::try_move(::gluino::string_cast<typename yaml_iarchive::value_type>(key))};
                    }
                    return *this;
                }

                inline iterator operator--(int) noexcept { return iterator(_node.prev_sibling()); }

                [[nodiscard]] inline const value_type& operator*() const noexcept { return _key; }

                [[nodiscard]] inline const value_type* operator->() const noexcept { return &_key; }

                [[nodiscard]] inline bool operator==(const iterator& other) const {
                    return _node.id() == other._node.id() || !_node.valid() && !other._node.valid();
                }

               private:
                inline explicit iterator(::ryml::NodeRef node) noexcept : _node(node) {
                    if (node.valid()) {
                        ::std::string_view key(_node.key().str, _node.key().len);
                        _key = {"", ::gluino::try_move(::gluino::string_cast<typename yaml_iarchive::value_type>(key))};
                    }
                }

                ::ryml::NodeRef _node;
                value_type _key;

                friend class yaml_iarchive;
            };

            inline yaml_iarchive(::ryml::NodeRef node,
                                 ::articuno::ryml::yaml_source<parser_type, Flags, SerdeTag>& root) noexcept
                : _node(node), _root(root) {}

            [[nodiscard]] inline bool is_sequence() const noexcept { return _node.is_seq(); }

            [[nodiscard]] inline bool is_map() const noexcept { return _node.is_map(); }

            [[nodiscard]] bool is_bool() const noexcept {
                auto& val = _node.val();
                if (val.size() > 5) {
                    return false;
                }
                ::std::string c(val.str, val.size());
                return c == "y" || c == "Y" || c == "yes" || c == "Yes" || c == "YES" ||
                       c == "n" || c == "N" || c == "no" || c == "No" || c == "NO" ||
                       c == "true" || c == "True" || c == "TRUE" || c == "false" || c == "False" || c == "False" ||
                       c == "on" || c == "On" || c == "ON" || c == "off" || c == "Off" || c == "OFF";
            }

            [[nodiscard]] inline bool is_number() const noexcept {
                return _node.val().is_number();
            }

            [[nodiscard]] inline bool is_int() const noexcept {
                return _node.val().is_integer();
            }

            [[nodiscard]] inline bool is_signed_int() const noexcept {
                return _node.val().is_integer() && (_node.val().begins_with('+') || _node.val().begins_with('-'));
            }

            [[nodiscard]] inline bool is_unsigned_int() const noexcept {
                return _node.val().is_integer() && (!_node.val().begins_with('+') && !_node.val().begins_with('-'));
            }

            [[nodiscard]] bool is_null() const noexcept {
                auto& val = _node.val();
                if (val.empty()) {
                    return true;
                }
                if (val.size() > 4) {
                    return false;
                }
                ::std::string candidate(val.str, val.size());
                return candidate == "null" || candidate == "NULL" || candidate == "Null" || candidate == "~";
            }

            [[nodiscard]] inline bool is_float() const noexcept {
                return _node.val().is_number() && !_node.val().is_integer();
            }

            [[nodiscard]] bool is_string() const noexcept {
                return !is_null() && !is_bool() && !is_int() && !is_float() && !is_sequence() && !is_map();
            }

            [[nodiscard]] inline size_type size() const noexcept { return _node.num_children(); }

            [[nodiscard]] inline iterator begin() const noexcept { return iterator(_node.first_child()); }

            [[nodiscard]] inline iterator end() const noexcept { return iterator(::ryml::NodeRef()); }

            [[nodiscard]] inline iterator cbegin() const noexcept { return begin(); }

            [[nodiscard]] inline iterator cend() const noexcept { return end(); }

            template <::gluino::arithmetic T>
            bool operator<=>(::articuno::self<T>&& value) {
                if (_node.id() == ::ryml::NONE) {
                    return false;
                }
                ::std::conditional_t<
                    ::std::is_same_v<T, bool>, bool,
                    ::std::conditional_t<::std::is_floating_point_v<T>, double_t, ::gluino::widest_type_t<T>>>
                    out;
                _node >> out;
                ::articuno::conversion::convert(out, value.value(), value.flags());
                // track_anchor(value.value());
                return true;
            }

            template <class Char = char, class Traits = ::std::char_traits<Char>, class Alloc = ::std::allocator<Char>>
            bool operator<=>(::articuno::self<::std::basic_string<Char, Traits, Alloc>>&& value) {
                if (_node.id() == ::ryml::NONE) {
                    return false;
                }
                ::std::string temp;
                _node >> temp;
                value.value() = ::std::move(::gluino::string_cast<Char, Traits>(temp));
                // track_anchor(value.value());
                return true;
            }

            template <class Traits = ::std::char_traits<char>, class Alloc = ::std::allocator<char>>
            bool operator<=>(::articuno::self<::std::basic_string<char, Traits, Alloc>>&& value) {
                if (_node.id() == ::ryml::NONE || !_node.valid()) {
                    if (value.flags().all(::articuno::value_flags::required)) {
                        throw ::articuno::format_error("Missing required value.");
                    }
                    return false;
                }
                _node >> value.value();
                // track_anchor(value.value());
                return true;
            }

            template <class Traits = ::std::char_traits<char8_t>, class Alloc = ::std::allocator<char8_t>>
            inline bool operator<=>(::articuno::self<::std::basic_string<char8_t, Traits, Alloc>>&& value) {
                return *this <=> reinterpret_cast<::articuno::self<::std::basic_string<char>>&&>(value);
            }

            template <class T>
            requires(::articuno::custom_deserializable<T, yaml_iarchive>) bool operator<=>(
                ::articuno::self<T>&& value) {
                if (_node.id() == ::ryml::NONE) {
                    return false;
                }
                ::articuno::deserialize(*this, value.value(), value.flags());
                // track_anchor(value.value());
                return true;
            }

            template <class T, class Key, class Namespace>
            requires(::articuno::deserializable<T, yaml_iarchive>) bool operator<=>(
                ::articuno::kv<T, Key, Namespace>&& property) {
                if (!_node.is_map()) {
                    return false;
                }
                ::articuno::types_for(*this).template register_type<T>();
                decltype(auto) converted_key = ::gluino::string_cast<char>(property.key());
                auto child = _node.find_child(::ryml::to_csubstr(converted_key.data()));
                _last_child = child;
                if (child.valid()) {
                    yaml_iarchive ar(child, _root);
                    ar <=> ::articuno::self(property.value(), property.flags());
                    return true;
                } else if (property.flags().all(::articuno::value_flags::enum_type::required)) {
                    throw ::articuno::format_error(::std::format("Missing required field '{}'.", converted_key.data()));
                }
                return false;
            }

            template <class T, class Key, class Namespace>
            requires(::articuno::deserializable<T, yaml_iarchive>) bool operator<=>(
                ::articuno::index<T, Key, Namespace>&& property) {
                ::articuno::types_for(*this).template register_type<T>();
                auto child =
                    _node[property.position() >= 0 ? property.position() : _node.num_children() + property.position()];
                _last_child = child;
                if (child.valid()) {
                    yaml_iarchive archive(child, _root);
                    archive <=> ::articuno::self(property.value(), property.flags());
                    return true;
                } else if (property.flags().all(::articuno::value_flags::enum_type::required)) {
                    throw ::articuno::format_error(::std::format("Missing required index {}.", property.position()));
                }
                return false;
            }

            template <class T>
            requires(::articuno::deserializable<T, yaml_iarchive>) bool operator<=>(::articuno::next<T>&& value) {
                if (!_node.is_map() && !_node.is_seq()) {
                    return false;
                }
                ::articuno::types_for(*this).template register_type<T>();
                ::ryml::NodeRef child;
                if (!_last_child.has_value()) {
                    _last_child = child = _node.first_child();
                } else {
                    _last_child = child = _last_child.value().next_sibling();
                }
                if (child.valid()) {
                    yaml_iarchive ar(child, _root);
                    ar <=> ::articuno::self(value.value(), value.flags());
                    return true;
                } else if (value.flags().all(::articuno::value_flags::enum_type::required)) {
                    throw ::articuno::format_error("Missing required next value.");
                }
                return false;
            }

            bool operator<=>(::articuno::sequence&&) {
                return _node.is_seq();
            }

            bool operator<=>(::articuno::object&&) {
                return _node.is_map();
            }

            bool operator<=>(::articuno::null&&) { return true; }

           private:
            ::ryml::NodeRef _node;
            ::std::optional<::ryml::NodeRef> _last_child;
            ::articuno::ryml::yaml_source<Parser, Flags, SerdeTag>& _root;
        };

        using size_type = ::std::size_t;
        using parser_type = Parser;
        using value_type = typename parser_type::value_type;
        using traits_type = typename parser_type::traits_type;
        using stream_type = ::std::basic_istream<value_type, traits_type>;
        using archive_type = yaml_iarchive;

        template <::std::enable_if_t<::std::is_default_constructible_v<parser_type>, int> = 0>
        inline explicit yaml_source(stream_type& input) noexcept(noexcept(parser_type())) : _input(&input) {}

        inline yaml_source(stream_type& input, parser_type parser) noexcept(noexcept(_parser = parser))
            : _input(&input), _parser(parser) {}

        inline void reset(stream_type& input) { _input = &input; }

        template <class T>
        requires(::articuno::deserializable<T, archive_type>) yaml_source& operator>>(T& value) { return read(value); }

        template <class T>
        requires(::articuno::deserializable<T, archive_type>)
            yaml_source& read(T& value, ::articuno::value_flags flags = ::articuno::value_flags::allow_indirection) {
            // TODO: When vcpkg catches up to modern ryml versions, switch to per-parser callback overrides.
            ::ryml::set_callbacks(
                ::ryml::Callbacks(nullptr, nullptr, nullptr,
                                  [](const char* msg, ::std::size_t msg_len, ::ryml::Location location, void*) {
                                      throw ::articuno::format_error(
                                          ::std::format("Error deserializing YAML data at line {}, column {}: {}",
                                                        location.line, location.col, ::std::string_view(msg, msg_len)));
                                  }));
            auto tree = parse(false);
            archive_type ar(tree.rootref(), *this);
            ar <=> ::articuno::self(value, flags);
            ::ryml::reset_callbacks();
            return *this;
        }

        inline void skip(size_type count = 1) {
            for (size_type i = 0; i < count; ++i) {
                parse(true);
            }
        }

        inline void skip_up_to(size_type count = 1) {
            for (size_type i = 0; i < count; ++i) {
                if (_input->eof()) {
                    return;
                }
                parse(true);
            }
        }

       protected:
        void collect_anchors(::ryml::NodeRef node) {
            if (node.has_val_anchor()) {
                auto anchor = node.val_anchor();
                auto [_, success] = _anchors.try_emplace(::std::string_view(anchor.str, anchor.size()), node);
                if (!success) {
                    throw ::articuno::format_error(
                        ::std::format("Found duplicate anchor name '{}' "
                                      "in YAML document.",
                                      anchor.str));
                }
            }
            for (auto child : node.children()) {
                collect_anchors(child);
            }
        }

        [[nodiscard]] inline ::ryml::Tree parse(bool skip) {
            auto tree = _parser.parse(*_input);
            if (!skip) {
                if (Flags.all(::articuno::archive_flags::enum_type::disable_tracking)) {
                    tree.resolve();
                } else {
                    collect_anchors(tree.rootref());
                }
            }
            return ::std::move(tree);
        }

       private:
        stream_type* _input;
        parser_type _parser;
        ::phmap::flat_hash_map<::std::string_view, ::ryml::NodeRef> _anchors;
        ::phmap::flat_hash_map<::ryml::NodeRef, void*> _tracked;
    };
}  // namespace articuno::ryml
