#pragma once

#include "../articuno.h"

namespace articuno {
	template <class Char>
	using basic_zstring = Char*;

	using zstring = ::articuno::basic_zstring<char>;
	using czstring = ::articuno::basic_zstring<const char>;
	using wzstring = ::articuno::basic_zstring<wchar_t>;
	using cwzstring = ::articuno::basic_zstring<const wchar_t>;
	using u8zstring = ::articuno::basic_zstring<char8_t>;
	using cu8zstring = ::articuno::basic_zstring<const char8_t>;
	using u16zstring = ::articuno::basic_zstring<char16_t>;
	using cu16zstring = ::articuno::basic_zstring<const char16_t>;
	using u32zstring = ::articuno::basic_zstring<char32_t>;
	using cu32zstring = ::articuno::basic_zstring<const char32_t>;
}
