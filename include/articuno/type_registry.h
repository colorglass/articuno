#pragma once

#include <atomic>
#include <latch>

#include <gluino/abi/abi.h>
#include <gluino/autorun.h>
#include <gluino/map.h>
#include <gluino/ptr.h>
#include <gluino/typeinfo.h>
#include <parallel_hashmap/phmap.h>

#include "access.h"
#include "allocator.h"
#include "type_error.h"

namespace articuno {
	template <::articuno::archive Archive>
	class type_registry;

	template <::articuno::serializing_archive Archive, class T>
	inline void serialize(Archive& ar, T& value, ::articuno::value_flags flags);

	template <::articuno::deserializing_archive Archive, class T>
	inline void deserialize(Archive& ar, T& value, ::articuno::value_flags flags);

	template <class Archive>
	class serde_type_info {
	public:
		[[nodiscard]] inline ::std::type_index type_index() const noexcept {
			return _type_index;
		}

		[[nodiscard]] inline ::std::string_view name() const noexcept {
			return _name;
		}

		[[nodiscard]] inline void* allocate(Archive& ar, ::articuno::value_flags flags,
				::std::size_t count = 1) const noexcept {
			return _allocator(ar, flags, count);
		}

		inline void* construct_at(Archive& ar, void* mem, ::articuno::value_flags flags) const {
			if (!_constructor.has_value()) {
				throw ::articuno::type_error("No constructor available for polymorphic type.");
			}
			return _constructor.value()(ar, mem, flags);
		}

		inline void serialize(Archive& ar, const void* destination, ::articuno::value_flags flags) const {
			assert(_serialize.has_value());
			return _serialize.value()(ar, destination, flags);
		}

		inline void deserialize(Archive& ar, void* destination, ::articuno::value_flags flags) const {
			assert(_deserialize.has_value());
			return _deserialize.value()(ar, destination, flags);
		}

	private:
		using allocator_type = ::std::function<void*(Archive&, ::articuno::value_flags, ::std::size_t)>;
		using constructor_type = ::std::function<void*(Archive&, void*, ::articuno::value_flags)>;

		inline explicit serde_type_info(::std::type_index type_index) noexcept
			: _type_index(type_index) {
		}

		::std::type_index _type_index;
		::std::string _name;
		allocator_type _allocator;
		::std::optional<constructor_type> _constructor;
		::std::optional<::std::function<void(Archive&, const void*, ::articuno::value_flags)>> _serialize;
		::std::optional<::std::function<void(Archive&, void*, ::articuno::value_flags)>> _deserialize;

		template <::articuno::archive>
		friend class ::articuno::type_registry;

		template <class, class>
		friend struct ::std::pair;
	};

        /**
         * Represents a delayed type registration.
         *
         * <p>
         * The <code>type_registration</code> captures the logic to perform type
         * registration with type erasure. The purpose of an instance is to
         * allow registering a type to another library or module's type registry
         * when a library wants to expose extensibility in type registration.
         * Such libraries can expose a public API to accept a reference to a
         * <code>type_registration</code> and then use it to register a type
         * from a third-party library to its own type registries.
         * </p>
         */
        class type_registration {
        public:
            template <::articuno::archive Archive, class T>
            [[nodiscard]] static type_registration create() {
                ::articuno::type_registration result;
                result._register = [](void* registry) {
                    auto* types = reinterpret_cast<::articuno::type_registry<Archive>*>(registry);
                    return types->template register_type<T>();
                };
                return result;
            }

        private:
            inline type_registration() noexcept = default;

            template <::articuno::archive Archive>
            inline bool perform_registration(::articuno::type_registry<Archive>& registry) const {
              return _register(reinterpret_cast<void*>(&registry));
            }

            ::std::function<bool(void* registry)> _register;

            template <::articuno::archive Archive>
            friend class type_registry;
        };

	template <::articuno::archive Archive>
	class type_registry {
	public:
		using allocator_type = typename ::articuno::serde_type_info<Archive>::allocator_type;
		using constructor_type = typename ::articuno::serde_type_info<Archive>::constructor_type;

                bool register_type(const type_registration& registration) {
                    return registration.perform_registration(*this);
                }

		template <class T>
		bool register_type() {
			return register_type<::gluino::uncvref_t<T>>(get_default_allocator<::gluino::uncvref_t<T>>());
		}

		template <class T>
		bool register_type(allocator_type allocator, constructor_type constructor) {
			return register_type<::gluino::uncvref_t<T>>(::std::move(allocator),
				::std::make_optional(::std::move(constructor)));
		}

		template <class T>
		bool register_type(allocator_type allocator) {
			if constexpr (::std::default_initializable<::gluino::uncvref_t<T>>) {
				return register_type<::gluino::uncvref_t<T>>(::std::move(allocator),
					get_default_constructor<::gluino::uncvref_t<T>>());
			} else {
				return register_type<::gluino::uncvref_t<T>>(::std::move(allocator), ::std::optional<constructor_type>{});
			}
		}

		template <class T>
		bool register_type(constructor_type constructor) {
			return register_type(get_default_allocator<::gluino::uncvref_t<T>>(), ::std::move(constructor));
		}

		template <class T>
		requires (::articuno::primitive<::gluino::uncvref_t<T>, Archive>)
		inline bool register_type(allocator_type, constructor_type) {
			return false;
		}

		template <class T>
		requires (::articuno::primitive<::gluino::uncvref_t<T>, Archive>)
		inline bool register_type(constructor_type) {
			return false;
		}

		template <class T>
		requires (::articuno::primitive<::gluino::uncvref_t<T>, Archive>)
		inline bool register_type(allocator_type) {
			return false;
		}

		template <class T>
		requires (::articuno::primitive<::gluino::uncvref_t<T>, Archive>)
		::gluino::optional_ptr<const ::articuno::serde_type_info<Archive>> lookup_type(T&& object) noexcept {
			return {};
		}

		template <class T>
		::gluino::optional_ptr<const ::articuno::serde_type_info<Archive>> lookup_type(T&&) noexcept {
			::std::type_index index{::gluino::type_index<::gluino::uncvref_t<T>>()};
			auto[result, success] = _types_by_index.try_emplace(index);
			if (!success) {
				return result->second.get();
			}
			if constexpr (::std::default_initializable<::gluino::uncvref_t<T>>) {
				return register_type<::gluino::uncvref_t<T>>(get_default_allocator<T>(),
				        get_default_constructor<::gluino::uncvref_t<T>>(), result);
			} else {
				return register_type<::gluino::uncvref_t<T>>(get_default_allocator<::gluino::uncvref_t<T>>(),
				        std::optional<constructor_type>(), result);
			}
		}

		template <::gluino::polymorphic T>
		::gluino::optional_ptr<const ::articuno::serde_type_info<Archive>> lookup_type(T&& object) noexcept {
			register_type<::gluino::uncvref_t<T>>();
			auto result = _types_by_index.find(::gluino::type_index(object));
			if (result == _types_by_index.end()) {
				return {};
			}
			return result->second.get();
		}

		::gluino::optional_ptr<const ::articuno::serde_type_info<Archive>> lookup_type_name(
			::std::string_view name) const noexcept {
			auto result = _types_by_name.find(name);
			if (result == _types_by_name.end()) {
				return {};
			}
			return result->second;
		}

		template <class T>
		::gluino::optional_ptr<const ::articuno::serde_type_info<Archive>> lookup_type() {
			auto result = _types_by_index.find(::gluino::type_index<::gluino::uncvref_t<T>>());
			if (result == _types_by_index.end()) {
				return {};
			}
			return result->second.get();
		}

		template <class T>
		requires (::articuno::primitive<T, Archive>)
		::gluino::optional_ptr<const ::articuno::serde_type_info<Archive>> lookup_type() {
			return {};
		}

		[[nodiscard]] static inline type_registry<typename Archive::archive_type>& get() {
			return ::gluino::lazy_singleton<::std::decay_t<type_registry<typename Archive::archive_type>>>::value();
		}

	protected:
		inline type_registry() noexcept = default;

	private:
		template <class T>
		bool register_type(allocator_type allocator, ::std::optional<constructor_type> constructor) {
			::std::type_index index{::gluino::type_index<T>()};
			auto[result, success] = _types_by_index.try_emplace(index);
			if (!success) {
				return false;
			}
			register_type<T>(::std::move(allocator), ::std::move(constructor), result);
			return true;
		}

		template <class T>
		static allocator_type& get_default_allocator() {
			static allocator_type def = [](Archive& ar, ::articuno::value_flags flags, ::std::size_t count) {
				return ::articuno::serde::allocate<Archive, T>(ar, flags, count);
			};
			return def;
		}

		template <class T>
		static constructor_type& get_default_constructor() {
			static constructor_type def = [](Archive& ar, void* mem, ::articuno::value_flags flags) {
				return reinterpret_cast<void*>(::articuno::serde::construct_at<Archive, ::gluino::uncvref_t<T>>(
					ar, reinterpret_cast<T*>(mem), flags));
				};
			return def;
		}

		::phmap::parallel_flat_hash_map<::std::type_index, ::std::unique_ptr<::articuno::serde_type_info<Archive>>,
			::std::hash<::std::type_index>, ::std::equal_to<::std::type_index>,
			::std::allocator<::std::pair<::std::type_index, ::std::unique_ptr<::articuno::serde_type_info<Archive>>>>,
			4, ::std::mutex> _types_by_index;
		::phmap::parallel_flat_hash_map<::std::string_view, ::articuno::serde_type_info<Archive>*,
			::gluino::str_hash<true>, ::gluino::str_equal_to<>,
			::std::allocator<::std::pair<::std::string_view, ::articuno::serde_type_info<Archive>*>>,
			4, ::std::mutex> _types_by_name;

		template <class T>
		::articuno::serde_type_info<Archive>* register_type(allocator_type allocator, ::std::optional<constructor_type> constructor,
				typename decltype(_types_by_index)::iterator iter) {
			iter->second.reset(new ::articuno::serde_type_info<Archive>(iter->first));
			auto& type = *iter->second;
			type._name = ::gluino::abi::demangle(iter->first.name());
			type._allocator = ::std::move(allocator);
			type._constructor = ::std::move(constructor);
			if constexpr (::articuno::serializable<T, Archive>) {
				type._serialize = [](Archive& ar, const void* value, ::articuno::value_flags flags) {
					const auto& val = *reinterpret_cast<const T*>(value);
					::articuno::access::serde<Archive, T>(ar, val, flags);
				};
			}
			if constexpr (::articuno::deserializable<T, Archive>) {
				type._deserialize = [](Archive& ar, void* value, ::articuno::value_flags flags) {
					::articuno::access::serde<Archive, T>(ar, *reinterpret_cast<T*>(value), flags);
				};
			}
			_types_by_name[type._name] = &type;
			return iter->second.get();
		}

		template <class T, class Tag>
		friend class ::gluino::lazy_singleton;
	};

	template <class Archive>
	[[nodiscard]] inline auto& types_for(Archive&) {
		return ::articuno::type_registry<typename Archive::archive_type>::get();
	}

	template <::articuno::serializing_archive Archive, class T>
	void serialize(Archive& ar, T& value, ::articuno::value_flags flags) {
		if constexpr (::gluino::polymorphic<T>) {
			auto tinfo =::articuno::types_for(ar).lookup_type(value);
			if (tinfo.is_null()) {
				throw ::articuno::type_error("Unable to lookup polymorphic type.");
			}
			tinfo->serialize(ar, &value, flags);
		} else {
			::articuno::access::serde<Archive, T>(ar, value, flags);
		}
	}

	template <::articuno::deserializing_archive Archive, class T>
	void deserialize(Archive& ar, T& value, ::articuno::value_flags flags) {
		auto tinfo =::articuno::types_for(ar).lookup_type(value);
		if constexpr (::gluino::polymorphic<T>) {
			if (tinfo.is_null()) {
				throw ::articuno::type_error("Unable to lookup polymorphic type.");
			}
			tinfo->deserialize(ar, &value, flags);
		} else {
			::articuno::access::serde<Archive, T>(ar, value, flags);
		}
	}
}
