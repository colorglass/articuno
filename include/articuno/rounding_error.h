#pragma once

#include "format_error.h"

namespace articuno {
    class rounding_error : public ::articuno::format_error {
    public:
        using ::articuno::format_error::format_error;
    };
}
