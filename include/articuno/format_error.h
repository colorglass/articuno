#pragma once

#include "serde_error.h"

namespace articuno {
    class format_error : public ::articuno::runtime_serde_error {
    public:
        using ::articuno::runtime_serde_error::runtime_serde_error;
    };
}
