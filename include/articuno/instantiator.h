#pragma once

#include <gluino/string_cast.h>

#include "allocator.h"
#include "kv.h"
#include "type_error.h"
#include "type_registry.h"

namespace articuno::serde {
	template <::articuno::deserializing_archive Archive, class T>
	T* instantiate(Archive& ar, ::articuno::value_flags flags, ::std::size_t count = 1) {
		::gluino::optional_ptr<const ::articuno::serde_type_info<Archive>> tinfo;
		::articuno::types_for(ar).template register_type<T>();
		if constexpr (::gluino::is_ultimately_polymorphic_v<T>) {
			::std::string type;
			ar <=> ::articuno::kv(type, "__type", flags);
			decltype(auto) type_name = ::gluino::string_cast<char>(type);
			tinfo = ::articuno::types_for(ar).lookup_type_name(type_name);
			if (tinfo.is_null()) {
				throw ::articuno::type_error(
					::std::format("Unable to find type information for type '{}'.", type_name));
			}
		} else {
			tinfo = ::articuno::types_for(ar).template lookup_type<T>();
			if (tinfo.is_null()) {
				return reinterpret_cast<T*>(::articuno::serde::allocate<Archive, T>(ar, flags, count));
			}
		}

		auto* memory = reinterpret_cast<::gluino::unconst_t<T*>>(tinfo->allocate(ar, flags, count));
		for (std::size_t i = 0; i < count; ++i) {
			tinfo->construct_at(ar, memory + i, flags);
		}
		return reinterpret_cast<T*>(memory);
	}
}
