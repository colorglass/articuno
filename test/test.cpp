#include <articuno/articuno.h>
#include <articuno/types/auto.h>
#include <articuno/archives/ryml/ryml.h>

#include <articuno/types/tsl/ordered_map.h>
#include <articuno/types/tsl/ordered_set.h>

#include <stdexcept>

using namespace articuno;
using namespace articuno::ryml;

struct Bar {
	articuno_serde(ar) {
		ar <=> kv(x, "x", value_flags::allow_indirection);
	}

	std::stringstream x{};
};

class Foo {
public:
	articuno_serde(ar) {
		ar <=> kv(val, "val", value_flags::allow_indirection);
	}

	static_assert(std::same_as<gluino::unconst_t<Bar* const&>, Bar*&>);
	Bar* val{new Bar{}};
};

void Testing() {
	std::stringstream in;
	in.str("val:\n  x: Testing");
	yaml_source<> source(in);
	Foo* f;
	source >> f;

	std::stringstream out;
	Foo f2;
	yaml_sink<> sink(out);
	sink << f2;
	auto str = out.str();
}

int main() {
	Testing();
	return 0;
}
