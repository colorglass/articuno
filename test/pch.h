#pragma once

// Include all implemented STL.
#include <cassert>
#include <cctype>
#include <cerrno>
#include <cfenv>
#include <cfloat>
#include <cinttypes>
#include <climits>
#include <clocale>
#include <cmath>
#include <csetjmp>
#include <csignal>
#include <cstdarg>
#include <cstddef>
#include <cstdint>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <ctime>
#include <cuchar>
#include <cwchar>
#include <cwctype>

#include <algorithm>
#include <any>
#include <array>
#include <atomic>
#include <bitset>
#include <charconv>
#include <chrono>
#include <complex>
#include <condition_variable>
#include <deque>
#include <exception>
#include <execution>
#include <filesystem>
#include <forward_list>
#include <fstream>
#include <functional>
#include <future>
#include <initializer_list>
#include <iomanip>
#include <iosfwd>
#include <ios>
#include <iostream>
#include <istream>
#include <iterator>
#include <limits>
#include <locale>
#include <map>
#include <memory>
#include <memory_resource>
#include <mutex>
#include <new>
#include <numeric>
#include <optional>
#include <ostream>
#include <queue>
#include <random>
#include <regex>
#include <ratio>
#include <scoped_allocator>
#include <set>
#include <shared_mutex>
#include <sstream>
#include <stack>
#include <stdexcept>
#include <streambuf>
#include <string>
#include <string_view>
#include <system_error>
#include <thread>
#include <tuple>
#include <typeindex>
#include <typeinfo>
#include <type_traits>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <valarray>
#include <variant>
#include <vector>

#if _MSVC_LANG > 201703L
#include <barrier>
#include <bit>
#include <compare>
#include <concepts>
#include <coroutine>
#include <format>
#include <latch>
#include <numbers>
#include <ranges>
#include <semaphore>
#include <source_location>
#include <span>
#include <syncstream>
#include <version>
#endif

// Bundled third-party content.
#include <parallel_hashmap/btree.h>
#include <parallel_hashmap/meminfo.h>
#include <parallel_hashmap/phmap.h>
#include <parallel_hashmap/phmap_base.h>
#include <parallel_hashmap/phmap_bits.h>
#include <parallel_hashmap/phmap_config.h>
#include <parallel_hashmap/phmap_dump.h>
#include <parallel_hashmap/phmap_fwd_decl.h>
#include <parallel_hashmap/phmap_utils.h>

// Core dependencies.
#include <gluino/gluino.h>

// Archive dependencies.
//#include <jsoncons/json.hpp>
//#include <jsoncons_ext/bson/bson.hpp>
//#include <jsoncons_ext/ubjson/ubjson.hpp>
//#include <jsoncons_ext/cbor/cbor.hpp>
//#include <jsoncons_ext/msgpack/msgpack.hpp>
//#include <pugixml.hpp>
#include <ryml/ryml.hpp>
#include <ryml/ryml_std.hpp>
//#include <toml.hpp>

// Instructions for Google Test on how to print char8_t strings.
// This is built into Google Test but needed for the Vcpkg build which does not enable char8_t support.
namespace std {
	inline std::ostream& operator<<(std::ostream& os, const std::u8string& value) {
		return os << reinterpret_cast<const std::string&>(value);
	}

	inline std::ostream& operator<<(std::ostream& os, const std::u8string_view value) {
		return os << reinterpret_cast<const std::string_view&>(value);
	}

	inline std::ostream& operator<<(std::ostream& os, const char8_t* value) {
		return os << reinterpret_cast<const char*>(value);
	}
}
